-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 23, 2018 at 06:01 AM
-- Server version: 5.6.39
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etlrobot_wp`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB AUTO_INCREMENT=308 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://52.15.241.137/etlrobot_wp', 'yes'),
(2, 'home', 'http://52.15.241.137/etlrobot_wp', 'yes'),
(3, 'blogname', 'ETL Robot', 'yes'),
(4, 'blogdescription', 'ETL Robot | Wordpress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'sreejith@spericorn.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:87:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=5&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:21:"attachments/index.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'etlrobot_theme', 'yes'),
(41, 'stylesheet', 'etlrobot_theme', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'identicon', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '5', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:2:{s:19:"wp_inactive_widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'cron', 'a:5:{i:1527022846;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1527069713;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1527069714;a:1:{s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1527069719;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(110, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1523613933;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(114, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.6.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.6.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.6-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.6-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.9.6-partial-5.zip";s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.6";s:7:"version";s:5:"4.9.6";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.9.5";}}s:12:"last_checked";i:1526997954;s:15:"version_checked";s:5:"4.9.5";s:12:"translations";a:0:{}}', 'no'),
(116, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1526997955;s:7:"checked";a:1:{s:21:"attachments/index.php";s:5:"3.5.9";}s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:1:{s:21:"attachments/index.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/attachments";s:4:"slug";s:11:"attachments";s:6:"plugin";s:21:"attachments/index.php";s:11:"new_version";s:5:"3.5.9";s:3:"url";s:42:"https://wordpress.org/plugins/attachments/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/attachments.3.5.9.zip";s:5:"icons";a:1:{s:7:"default";s:62:"https://s.w.org/plugins/geopattern-icon/attachments_fafafa.svg";}s:7:"banners";a:2:{s:2:"2x";s:66:"https://ps.w.org/attachments/assets/banner-1544x500.png?rev=636285";s:2:"1x";s:65:"https://ps.w.org/attachments/assets/banner-772x250.png?rev=636285";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(119, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1526997955;s:7:"checked";a:1:{s:14:"etlrobot_theme";s:3:"1.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(122, 'can_compress_scripts', '1', 'no'),
(140, 'current_theme', 'etlrobot_theme', 'yes'),
(141, 'theme_mods_etlrobot_theme', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(142, 'theme_switched', '', 'yes'),
(147, 'WPLANG', '', 'yes'),
(148, 'new_admin_email', 'sreejith@spericorn.com', 'yes'),
(157, 'recently_activated', 'a:0:{}', 'yes'),
(160, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(306, '_site_transient_timeout_theme_roots', '1526999755', 'no'),
(307, '_site_transient_theme_roots', 'a:1:{s:14:"etlrobot_theme";s:7:"/themes";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=284 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(5, 5, '_edit_last', '1'),
(6, 5, '_edit_lock', '1523613590:1'),
(7, 7, '_edit_last', '1'),
(8, 7, '_edit_lock', '1523613598:1'),
(9, 9, '_edit_last', '1'),
(10, 9, '_edit_lock', '1523613627:1'),
(47, 15, '_menu_item_type', 'post_type'),
(48, 15, '_menu_item_menu_item_parent', '0'),
(49, 15, '_menu_item_object_id', '9'),
(50, 15, '_menu_item_object', 'page'),
(51, 15, '_menu_item_target', ''),
(52, 15, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(53, 15, '_menu_item_xfn', ''),
(54, 15, '_menu_item_url', ''),
(56, 16, '_menu_item_type', 'post_type'),
(57, 16, '_menu_item_menu_item_parent', '0'),
(58, 16, '_menu_item_object_id', '7'),
(59, 16, '_menu_item_object', 'page'),
(60, 16, '_menu_item_target', ''),
(61, 16, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(62, 16, '_menu_item_xfn', ''),
(63, 16, '_menu_item_url', ''),
(65, 17, '_menu_item_type', 'post_type'),
(66, 17, '_menu_item_menu_item_parent', '0'),
(67, 17, '_menu_item_object_id', '5'),
(68, 17, '_menu_item_object', 'page'),
(69, 17, '_menu_item_target', ''),
(70, 17, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(71, 17, '_menu_item_xfn', ''),
(72, 17, '_menu_item_url', ''),
(74, 18, '_menu_item_type', 'custom'),
(75, 18, '_menu_item_menu_item_parent', '0'),
(76, 18, '_menu_item_object_id', '18'),
(77, 18, '_menu_item_object', 'custom'),
(78, 18, '_menu_item_target', ''),
(79, 18, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(80, 18, '_menu_item_xfn', ''),
(81, 18, '_menu_item_url', ''),
(83, 19, '_menu_item_type', 'custom'),
(84, 19, '_menu_item_menu_item_parent', '0'),
(85, 19, '_menu_item_object_id', '19'),
(86, 19, '_menu_item_object', 'custom'),
(87, 19, '_menu_item_target', ''),
(88, 19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(89, 19, '_menu_item_xfn', ''),
(90, 19, '_menu_item_url', ''),
(92, 20, '_menu_item_type', 'custom'),
(93, 20, '_menu_item_menu_item_parent', '0'),
(94, 20, '_menu_item_object_id', '20'),
(95, 20, '_menu_item_object', 'custom'),
(96, 20, '_menu_item_target', ''),
(97, 20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(98, 20, '_menu_item_xfn', ''),
(99, 20, '_menu_item_url', ''),
(104, 22, '_edit_last', '1'),
(105, 22, '_edit_lock', '1525874154:1'),
(106, 22, 'link_text', ''),
(107, 22, 'link', ''),
(108, 23, '_edit_last', '1'),
(109, 23, '_edit_lock', '1525874180:1'),
(110, 23, 'text_one', 'WHAT IS CLOUD ETL?'),
(111, 23, 'desc_one', 'ETL stands for Extract, Transform, and Load. ETLRobot is a Cloud service ETL'),
(112, 23, 'text_two', 'WHAT IS CLOUD ETL?'),
(113, 23, 'desc_two', 'ETL stands for Extract, Transform, and Load. ETLRobot is a Cloud service ETL'),
(114, 23, 'text_three', 'IT IS FULLY SECURE'),
(115, 23, 'desc_three', 'ETL stands for Extract, Transform, and Load. ETLRobot is a Cloud service ETL'),
(116, 23, 'text_four', 'IT IS FULLY SECURE'),
(117, 23, 'desc_four', 'ETL stands for Extract, Transform, and Load. ETLRobot is a Cloud service ETL'),
(121, 25, '_edit_last', '1'),
(122, 25, '_edit_lock', '1524140182:1'),
(126, 25, 'link_text', 'GET STARTED'),
(127, 25, 'link', 'https://www.google.co.in'),
(128, 25, 'trial_text', 'Start your 14-day free trial today!'),
(129, 25, 'img_text', 'BUILDING A BEAUTIFUL IS THE BUSINESS APP IN A HERE DUMMY SWIFT AND EFFICIENT IS THE HERE IS MANNER'),
(130, 27, '_edit_last', '1'),
(131, 27, '_edit_lock', '1523626362:1'),
(132, 27, 'allSubs', 'a:4:{i:1;a:5:{s:4:"name";s:5:"Basic";s:4:"rate";s:11:"$99 / MONTH";s:10:"highlights";a:4:{i:0;s:24:"5 Million Rows Per Month";i:1;s:21:"Standard Integrations";i:2;s:12:"Chat Support";i:3;s:16:"$20/Million Over";}s:4:"text";s:9:"START NOW";s:4:"link";s:18:"javascript:void(0)";}i:2;a:5:{s:4:"name";s:8:"Advanced";s:4:"rate";s:12:"$449 / MONTH";s:10:"highlights";a:4:{i:0;s:26:"100 Million Rows Per Month";i:1;s:21:"Standard Integrations";i:2;s:18:"Chat/Phone Support";i:3;s:16:"$10/Million Over";}s:4:"text";s:9:"START NOW";s:4:"link";s:18:"javascript:void(0)";}i:3;a:5:{s:4:"name";s:4:"Hero";s:4:"rate";s:12:"$899 / MONTH";s:10:"highlights";a:4:{i:0;s:26:"250 Million Rows Per Month";i:1;s:33:"Standard and Premium Integrations";i:2;s:16:"Priority Support";i:3;s:15:"$5/Million Over";}s:4:"text";s:9:"START NOW";s:4:"link";s:18:"javascript:void(0)";}i:4;a:5:{s:4:"name";s:10:"Enterprise";s:4:"rate";s:11:"CUSTOM PLAN";s:10:"highlights";a:4:{i:0;s:8:"Billions";i:1;s:16:"All Integrations";i:2;s:15:"Hotline Support";i:3;s:7:"Per SLA";}s:4:"text";s:9:"START NOW";s:4:"link";s:18:"javascript:void(0)";}}'),
(133, 29, '_wp_attached_file', '2018/04/bg-man.png'),
(134, 29, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:594;s:6:"height";i:556;s:4:"file";s:18:"2018/04/bg-man.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bg-man-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:18:"bg-man-300x281.png";s:5:"width";i:300;s:6:"height";i:281;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(135, 30, '_wp_attached_file', '2018/04/bg-img1.jpg'),
(136, 30, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:431;s:6:"height";i:285;s:4:"file";s:19:"2018/04/bg-img1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"bg-img1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"bg-img1-300x198.jpg";s:5:"width";i:300;s:6:"height";i:198;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(137, 31, '_wp_attached_file', '2018/04/bg-img2.jpg'),
(138, 31, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:431;s:6:"height";i:285;s:4:"file";s:19:"2018/04/bg-img2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"bg-img2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"bg-img2-300x198.jpg";s:5:"width";i:300;s:6:"height";i:198;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(139, 32, '_wp_attached_file', '2018/04/bg-img3.jpg'),
(140, 32, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:431;s:6:"height";i:285;s:4:"file";s:19:"2018/04/bg-img3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"bg-img3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"bg-img3-300x198.jpg";s:5:"width";i:300;s:6:"height";i:198;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(141, 22, '_thumbnail_id', '29'),
(142, 22, '_wp_old_slug', 'xno-coding-or-maintenances'),
(143, 22, 'attachments', '{"hpBannerAttach":[{"id":"30","fields":{"caption":"SAMPLE DUMMY TEXT"}},{"id":"31","fields":{"caption":"SAMPLE DUMMY TEXT"}},{"id":"32","fields":{"caption":"SAMPLE DUMMY TEXT"}}]}'),
(144, 22, 'youtube_caption', 'SAMPLE DUMMY TEXT'),
(145, 22, 'youtube_video', 'e-P5IFTqB98'),
(146, 33, '_wp_attached_file', '2018/04/abt-img.png'),
(147, 33, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:409;s:6:"height";i:421;s:4:"file";s:19:"2018/04/abt-img.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"abt-img-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"abt-img-291x300.png";s:5:"width";i:291;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(148, 23, '_thumbnail_id', '61'),
(149, 23, '_wp_old_slug', 'xabout-us'),
(150, 34, '_wp_attached_file', '2018/04/bus-bg.jpg'),
(151, 34, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:624;s:6:"height";i:695;s:4:"file";s:18:"2018/04/bus-bg.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"bus-bg-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"bus-bg-269x300.jpg";s:5:"width";i:269;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(152, 25, '_thumbnail_id', '34'),
(153, 25, '_wp_old_slug', 'xbuilding-a-beautiful-business-in-a-swift-and-efficient-manner'),
(154, 35, '_edit_last', '1'),
(155, 35, '_edit_lock', '1524140255:1'),
(156, 36, '_wp_attached_file', '2018/04/d1.png'),
(157, 36, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:168;s:4:"file";s:14:"2018/04/d1.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"d1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:14:"d1-300x168.png";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(158, 37, '_wp_attached_file', '2018/04/d2.png'),
(159, 37, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:168;s:4:"file";s:14:"2018/04/d2.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"d2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:14:"d2-300x168.png";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(160, 38, '_wp_attached_file', '2018/04/d3.png'),
(161, 38, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:168;s:4:"file";s:14:"2018/04/d3.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"d3-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:14:"d3-300x168.png";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(162, 39, '_wp_attached_file', '2018/04/d4.png'),
(163, 39, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:168;s:4:"file";s:14:"2018/04/d4.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"d4-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:14:"d4-300x168.png";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(164, 40, '_wp_attached_file', '2018/04/d5.png'),
(165, 40, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:168;s:4:"file";s:14:"2018/04/d5.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"d5-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:14:"d5-300x168.png";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(166, 35, 'attachments', '{"hpAdvertisingAttach":[{"id":"36","fields":{"name":"01"}},{"id":"37","fields":{"name":"02"}}],"hpSocialAttach":[{"id":"38","fields":{"name":"01"}},{"id":"39","fields":{"name":"02"}}],"hpAnalyticsAttach":[{"id":"40","fields":{"name":"01"}}]}'),
(167, 41, '_edit_last', '1'),
(168, 41, '_edit_lock', '1524140281:1'),
(169, 42, '_edit_last', '1'),
(170, 42, '_edit_lock', '1524140317:1'),
(171, 43, '_wp_attached_file', '2018/04/about-img.png'),
(172, 43, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:654;s:6:"height";i:475;s:4:"file";s:21:"2018/04/about-img.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"about-img-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"about-img-300x218.png";s:5:"width";i:300;s:6:"height";i:218;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(173, 42, '_thumbnail_id', '43'),
(174, 42, 'link_text', 'GET STARTED'),
(175, 42, 'link', 'https://www.google.co.in'),
(176, 44, '_edit_last', '1'),
(177, 44, '_edit_lock', '1524140374:1'),
(178, 44, 'text_one', 'WHAT IS CLOUD ETL?'),
(179, 44, 'desc_one', 'ETL stands for Extract, Transform, and Load. ETLRobot is a here Cloud service ETL engine to simplify and automate the iris of of capturing and structuring your data'),
(180, 44, 'text_two', 'EASY AS PIE'),
(181, 44, 'desc_two', 'ETL stands for Extract, Transform, and Load. ETLRobot is a here Cloud service ETL engine to simplify and automate the iris of of capturing and structuring your data'),
(182, 44, 'text_three', 'DATA WITHOUT WALLS'),
(183, 44, 'desc_three', 'ETL stands for Extract, Transform, and Load. ETLRobot is a here Cloud service ETL engine to simplify and automate the iris of of capturing and structuring your data'),
(184, 44, 'text_four', 'SECURE'),
(185, 44, 'desc_four', 'ETL stands for Extract, Transform, and Load. ETLRobot is a here Cloud service ETL engine to simplify and automate the iris of of capturing and structuring your data'),
(186, 45, '_edit_last', '1'),
(187, 45, '_edit_lock', '1524140416:1'),
(188, 45, '_thumbnail_id', '34'),
(189, 45, 'trial_text', 'Start your 14-day free trial today!'),
(190, 45, 'img_text', 'BUILDING A BEAUTIFUL IS THE BUSINESS APP IN A HERE DUMMY SWIFT AND EFFICIENT IS THE HERE IS MANNER'),
(191, 45, 'link_text', 'GET STARTED'),
(192, 45, 'link', 'https://www.google.co.in'),
(193, 46, '_edit_last', '1'),
(194, 46, '_edit_lock', '1524140437:1'),
(195, 48, '_edit_last', '1'),
(196, 48, '_edit_lock', '1524140498:1'),
(197, 49, '_wp_attached_file', '2018/04/plan-bg.jpg'),
(198, 49, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1700;s:6:"height";i:663;s:4:"file";s:19:"2018/04/plan-bg.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"plan-bg-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"plan-bg-300x117.jpg";s:5:"width";i:300;s:6:"height";i:117;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"plan-bg-768x300.jpg";s:5:"width";i:768;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"plan-bg-1024x399.jpg";s:5:"width";i:1024;s:6:"height";i:399;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(199, 48, '_thumbnail_id', '49'),
(200, 48, 'link_text', 'START TRIAL'),
(201, 48, 'link', 'https://www.google.co.in'),
(202, 48, 'subCaption', 'SUBSCRIPTION PLANS'),
(203, 48, 'subDesc', 'Connecting to your source is as simple as a login process. If you need help,  you can also invite someone from your team'),
(204, 48, 'compCaption', 'COMPARE PLAN FEATURES'),
(205, 48, 'compDesc', 'Connecting to your source is as simple as a login process. If you need help, \r\nyou can also invite someone from your team'),
(206, 50, '_edit_last', '1'),
(207, 50, '_edit_lock', '1526372189:1'),
(208, 50, 'sub_linkText', 'Start Now '),
(209, 50, 'sub_link', 'https://www.google.com'),
(210, 50, 'sub_boxes', 'a:4:{i:1;a:1:{s:5:"title";s:24:"5 Million Rows Per Month";}i:2;a:1:{s:5:"title";s:21:"Standard Integrations";}i:3;a:1:{s:5:"title";s:12:"Chat Support";}i:4;a:1:{s:5:"title";s:16:"$20/Million Over";}}'),
(211, 50, 'basePrice', '99'),
(212, 50, 'rows', '5 Million'),
(213, 50, 'add_rows', '$20/Million Over'),
(214, 50, 'integrations', 'Standard'),
(215, 50, 'support', 'Chat'),
(216, 51, '_edit_last', '1'),
(217, 51, '_edit_lock', '1524140667:1'),
(218, 51, 'sub_linkText', 'Start Now'),
(219, 51, 'sub_link', 'https://www.google.com'),
(220, 51, 'sub_boxes', 'a:4:{i:1;a:1:{s:5:"title";s:26:"100 Million Rows Per Month";}i:2;a:1:{s:5:"title";s:21:"Standard Integrations";}i:3;a:1:{s:5:"title";s:18:"Chat/Phone Support";}i:4;a:1:{s:5:"title";s:16:"$10/Million Over";}}'),
(221, 51, 'basePrice', '499'),
(222, 51, 'rows', '100 Million'),
(223, 51, 'add_rows', '$10/Million Over'),
(224, 51, 'integrations', 'Standard'),
(225, 51, 'support', 'Chat/Phone'),
(226, 52, '_edit_last', '1'),
(227, 52, '_edit_lock', '1524140878:1'),
(228, 52, 'sub_linkText', 'Start Now'),
(229, 52, 'sub_link', 'https://www.google.com'),
(230, 52, 'sub_boxes', 'a:4:{i:1;a:1:{s:5:"title";s:26:"250 Million Rows Per Month";}i:2;a:1:{s:5:"title";s:31:"Standard & Premium Integrations";}i:3;a:1:{s:5:"title";s:16:"Priority Support";}i:4;a:1:{s:5:"title";s:15:"$5/Million Over";}}'),
(231, 52, 'basePrice', '899'),
(232, 52, 'rows', '250 Million'),
(233, 52, 'add_rows', '$5/Million Over'),
(234, 52, 'integrations', 'Standard and Premium'),
(235, 52, 'support', 'Priority'),
(236, 53, '_edit_last', '1'),
(237, 53, '_edit_lock', '1526359242:1'),
(238, 53, 'sub_linkText', 'Start Now'),
(239, 53, 'sub_link', 'https://www.google.com'),
(240, 53, 'sub_boxes', 'a:4:{i:1;a:1:{s:5:"title";s:8:"Billions";}i:2;a:1:{s:5:"title";s:16:"All Integrations";}i:3;a:1:{s:5:"title";s:15:"Hotline Support";}i:4;a:1:{s:5:"title";s:7:"Per SLA";}}'),
(241, 53, 'basePrice', 'Contact us'),
(242, 53, 'rows', 'Billions'),
(243, 53, 'add_rows', 'SLA'),
(244, 53, 'integrations', 'ALL'),
(245, 53, 'support', 'Hotline'),
(246, 54, '_edit_last', '1'),
(247, 54, '_edit_lock', '1524141416:1'),
(275, 59, '_edit_last', '1'),
(276, 59, '_edit_lock', '1525787268:1'),
(277, 61, '_wp_attached_file', '2018/04/abt-img-1.png'),
(278, 61, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:339;s:6:"height";i:538;s:4:"file";s:21:"2018/04/abt-img-1.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"abt-img-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"abt-img-1-189x300.png";s:5:"width";i:189;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(279, 62, '_edit_last', '1'),
(280, 62, '_edit_lock', '1526368393:1'),
(281, 62, 'ft_address', 'Draper,\r\nUT, 84020, \r\nUnited States'),
(282, 62, 'ft_phone', '+1 801-870-7979'),
(283, 62, 'ft_email', 'info@etlrobotcom');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(5, 1, '2018-04-13 10:02:11', '2018-04-13 10:02:11', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-04-13 10:02:11', '2018-04-13 10:02:11', '', 0, 'http://52.15.241.137/etlrobot_wp/?page_id=5', 0, 'page', '', 0),
(6, 1, '2018-04-13 10:02:11', '2018-04-13 10:02:11', '', 'Home', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-04-13 10:02:11', '2018-04-13 10:02:11', '', 5, 'http://52.15.241.137/etlrobot_wp/2018/04/13/5-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2018-04-13 10:02:22', '2018-04-13 10:02:22', '', 'About', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2018-04-13 10:02:22', '2018-04-13 10:02:22', '', 0, 'http://52.15.241.137/etlrobot_wp/?page_id=7', 0, 'page', '', 0),
(8, 1, '2018-04-13 10:02:22', '2018-04-13 10:02:22', '', 'About', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2018-04-13 10:02:22', '2018-04-13 10:02:22', '', 7, 'http://52.15.241.137/etlrobot_wp/2018/04/13/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2018-04-13 10:02:30', '2018-04-13 10:02:30', '', 'Pricing', '', 'publish', 'closed', 'closed', '', 'pricing', '', '', '2018-04-13 10:02:30', '2018-04-13 10:02:30', '', 0, 'http://52.15.241.137/etlrobot_wp/?page_id=9', 0, 'page', '', 0),
(10, 1, '2018-04-13 10:02:30', '2018-04-13 10:02:30', '', 'Pricing', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-04-13 10:02:30', '2018-04-13 10:02:30', '', 9, 'http://52.15.241.137/etlrobot_wp/2018/04/13/9-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2018-04-13 10:41:41', '2018-04-13 10:41:41', ' ', '', '', 'publish', 'closed', 'closed', '', '15', '', '', '2018-04-19 12:40:59', '2018-04-19 12:40:59', '', 0, 'http://52.15.241.137/etlrobot_wp/?p=15', 3, 'nav_menu_item', '', 0),
(16, 1, '2018-04-13 10:41:41', '2018-04-13 10:41:41', '', 'About Us', '', 'publish', 'closed', 'closed', '', '16', '', '', '2018-04-19 12:40:59', '2018-04-19 12:40:59', '', 0, 'http://52.15.241.137/etlrobot_wp/?p=16', 5, 'nav_menu_item', '', 0),
(17, 1, '2018-04-13 10:41:41', '2018-04-13 10:41:41', ' ', '', '', 'publish', 'closed', 'closed', '', '17', '', '', '2018-04-19 12:40:59', '2018-04-19 12:40:59', '', 0, 'http://52.15.241.137/etlrobot_wp/?p=17', 1, 'nav_menu_item', '', 0),
(18, 1, '2018-04-13 10:45:47', '2018-04-13 10:45:47', '', 'Integrations', '', 'publish', 'closed', 'closed', '', 'integrations', '', '', '2018-04-19 12:40:59', '2018-04-19 12:40:59', '', 0, 'http://52.15.241.137/etlrobot_wp/?p=18', 2, 'nav_menu_item', '', 0),
(19, 1, '2018-04-13 10:45:48', '2018-04-13 10:45:48', '', 'How it Works', '', 'publish', 'closed', 'closed', '', 'how-it-works', '', '', '2018-04-19 12:40:59', '2018-04-19 12:40:59', '', 0, 'http://52.15.241.137/etlrobot_wp/?p=19', 4, 'nav_menu_item', '', 0),
(20, 1, '2018-04-13 10:45:48', '2018-04-13 10:45:48', '', 'Request a Demo', '', 'publish', 'closed', 'closed', '', 'request-a-demo', '', '', '2018-04-19 12:40:59', '2018-04-19 12:40:59', '', 0, 'http://52.15.241.137/etlrobot_wp/?p=20', 6, 'nav_menu_item', '', 0),
(22, 1, '2018-04-13 10:57:22', '2018-04-13 10:57:22', 'Connecting to your source is as simple as a login process. If you need help, you can also invite someone from your team', 'NO CODING OR MAINTENANCES', '', 'publish', 'closed', 'closed', '', 'no-coding-or-maintenances', '', '', '2018-05-09 13:03:12', '2018-05-09 13:03:12', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=hpbanner&#038;p=22', 0, 'hpbanner', '', 0),
(23, 1, '2018-04-13 11:34:47', '2018-04-13 11:34:47', 'Connecting to your source is as simple as a login process. If you need help, \r\nyou can also invite someone from your team', 'ABOUT US', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-05-09 13:58:30', '2018-05-09 13:58:30', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=hpabout&#038;p=23', 0, 'hpabout', '', 0),
(25, 1, '2018-04-13 12:01:39', '2018-04-13 12:01:39', 'Cooleaf is a web and mobile application letting teams find activities they love to empower shared interests and passions. Started from the scratch, now Cooleaf serves numerous enterprise cusapp during Atlanta Mobile Awards 2016.', 'Building a beautiful business in a swift and efficient manner', '', 'publish', 'closed', 'closed', '', 'building-a-beautiful-business-in-a-swift-and-efficient-manner', '', '', '2018-04-19 12:18:41', '2018-04-19 12:18:41', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=hpsection&#038;p=25', 0, 'hpsection', '', 0),
(27, 1, '2018-04-13 12:23:25', '2018-04-13 12:23:25', 'xConnecting to your source is as simple as a login process. If you need help, \r\nyou can also invite someone from your team', 'xSUBSCRIPTION PLANS', '', 'publish', 'closed', 'closed', '', 'xsubscription-plans', '', '', '2018-04-13 13:17:03', '2018-04-13 13:17:03', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=hpsubscription&#038;p=27', 0, 'hpsubscription', '', 0),
(28, 1, '2018-04-19 12:12:21', '2018-04-19 12:12:21', 'Connecting to your source is as simple as a login process. If you need help, you can also invite someone from your team', 'NO CODING OR MAINTENANCES', '', 'inherit', 'closed', 'closed', '', '22-autosave-v1', '', '', '2018-04-19 12:12:21', '2018-04-19 12:12:21', '', 22, 'http://52.15.241.137/etlrobot_wp/22-autosave-v1/', 0, 'revision', '', 0),
(29, 1, '2018-04-19 12:15:44', '2018-04-19 12:15:44', '', 'bg-man', '', 'inherit', 'open', 'closed', '', 'bg-man', '', '', '2018-04-19 12:15:44', '2018-04-19 12:15:44', '', 22, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/bg-man.png', 0, 'attachment', 'image/png', 0),
(30, 1, '2018-04-19 12:16:32', '2018-04-19 12:16:32', '', 'bg-img1', '', 'inherit', 'open', 'closed', '', 'bg-img1', '', '', '2018-04-19 12:16:32', '2018-04-19 12:16:32', '', 22, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/bg-img1.jpg', 0, 'attachment', 'image/jpeg', 0),
(31, 1, '2018-04-19 12:16:41', '2018-04-19 12:16:41', '', 'bg-img2', '', 'inherit', 'open', 'closed', '', 'bg-img2', '', '', '2018-04-19 12:16:41', '2018-04-19 12:16:41', '', 22, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/bg-img2.jpg', 0, 'attachment', 'image/jpeg', 0),
(32, 1, '2018-04-19 12:16:49', '2018-04-19 12:16:49', '', 'bg-img3', '', 'inherit', 'open', 'closed', '', 'bg-img3', '', '', '2018-04-19 12:16:49', '2018-04-19 12:16:49', '', 22, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/bg-img3.jpg', 0, 'attachment', 'image/jpeg', 0),
(33, 1, '2018-04-19 12:17:45', '2018-04-19 12:17:45', '', 'abt-img', '', 'inherit', 'open', 'closed', '', 'abt-img', '', '', '2018-04-19 12:17:45', '2018-04-19 12:17:45', '', 23, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/abt-img.png', 0, 'attachment', 'image/png', 0),
(34, 1, '2018-04-19 12:18:37', '2018-04-19 12:18:37', '', 'bus-bg', '', 'inherit', 'open', 'closed', '', 'bus-bg', '', '', '2018-04-19 12:18:37', '2018-04-19 12:18:37', '', 25, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/bus-bg.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2018-04-19 12:19:55', '2018-04-19 12:19:55', 'Connecting to your source is as simple as a login process. If you need help, \r\nyou can also invite someone from your team', 'DATA RESOURCES', '', 'publish', 'closed', 'closed', '', 'data-resources', '', '', '2018-04-19 12:19:55', '2018-04-19 12:19:55', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=hpresources&#038;p=35', 0, 'hpresources', '', 0),
(36, 1, '2018-04-19 12:19:14', '2018-04-19 12:19:14', '', 'd1', '', 'inherit', 'open', 'closed', '', 'd1', '', '', '2018-04-19 12:19:14', '2018-04-19 12:19:14', '', 35, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/d1.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2018-04-19 12:19:26', '2018-04-19 12:19:26', '', 'd2', '', 'inherit', 'open', 'closed', '', 'd2', '', '', '2018-04-19 12:19:26', '2018-04-19 12:19:26', '', 35, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/d2.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2018-04-19 12:19:35', '2018-04-19 12:19:35', '', 'd3', '', 'inherit', 'open', 'closed', '', 'd3', '', '', '2018-04-19 12:19:35', '2018-04-19 12:19:35', '', 35, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/d3.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2018-04-19 12:19:42', '2018-04-19 12:19:42', '', 'd4', '', 'inherit', 'open', 'closed', '', 'd4', '', '', '2018-04-19 12:19:42', '2018-04-19 12:19:42', '', 35, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/d4.png', 0, 'attachment', 'image/png', 0),
(40, 1, '2018-04-19 12:19:49', '2018-04-19 12:19:49', '', 'd5', '', 'inherit', 'open', 'closed', '', 'd5', '', '', '2018-04-19 12:19:49', '2018-04-19 12:19:49', '', 35, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/d5.png', 0, 'attachment', 'image/png', 0),
(41, 1, '2018-04-19 12:20:22', '2018-04-19 12:20:22', 'Connecting to your source is as simple as a login process. If you need help, you can also invite someone from your team', 'REQUEST A DEMO', '', 'publish', 'closed', 'closed', '', 'request-a-demo', '', '', '2018-04-19 12:20:22', '2018-04-19 12:20:22', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=hpdemo&#038;p=41', 0, 'hpdemo', '', 0),
(42, 1, '2018-04-19 12:20:57', '2018-04-19 12:20:57', 'Connecting to your source is as simple as a login process. If you need help, you can also invite someone from your team', 'GET YOUR DATA WITH ETLROBOT', '', 'publish', 'closed', 'closed', '', 'get-your-data-with-etlrobot', '', '', '2018-04-19 12:20:57', '2018-04-19 12:20:57', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=aboutbanner&#038;p=42', 0, 'aboutbanner', '', 0),
(43, 1, '2018-04-19 12:20:47', '2018-04-19 12:20:47', '', 'about-img', '', 'inherit', 'open', 'closed', '', 'about-img', '', '', '2018-04-19 12:20:47', '2018-04-19 12:20:47', '', 42, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/about-img.png', 0, 'attachment', 'image/png', 0),
(44, 1, '2018-04-19 12:21:52', '2018-04-19 12:21:52', 'Connecting to your source is as simple as a login process. If you need help, you can also invite someone from your team', 'ABOUT US', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-04-19 12:21:52', '2018-04-19 12:21:52', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=aboutabout&#038;p=44', 0, 'aboutabout', '', 0),
(45, 1, '2018-04-19 12:22:35', '2018-04-19 12:22:35', 'Cooleaf is a web and mobile application letting teams find activities they love to empower shared interests and passions. Started from the scratch, now Cooleaf serves numerous enterprise cusapp during Atlanta Mobile Awards 2016.', 'Building a beautiful business in a swift and efficient manner', '', 'publish', 'closed', 'closed', '', 'building-a-beautiful-business-in-a-swift-and-efficient-manner', '', '', '2018-04-19 12:22:35', '2018-04-19 12:22:35', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=aboutsection&#038;p=45', 0, 'aboutsection', '', 0),
(46, 1, '2018-04-19 12:22:57', '2018-04-19 12:22:57', 'To know more about pricing and features, feel free to contact us.', 'Have Any Queries?', '', 'publish', 'closed', 'closed', '', 'have-any-queries', '', '', '2018-04-19 12:22:57', '2018-04-19 12:22:57', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=aboutquery&#038;p=46', 0, 'aboutquery', '', 0),
(47, 1, '2018-04-19 12:23:00', '2018-04-19 12:23:00', 'To know more about pricing and features, feel free to contact us.', 'Have Any Queries?', '', 'inherit', 'closed', 'closed', '', '46-autosave-v1', '', '', '2018-04-19 12:23:00', '2018-04-19 12:23:00', '', 46, 'http://52.15.241.137/etlrobot_wp/46-autosave-v1/', 0, 'revision', '', 0),
(48, 1, '2018-04-19 12:23:56', '2018-04-19 12:23:56', 'Start off with a free 14 day trial – no credit card required Once you love the simplify, select a plan that suits you best.', 'GET YOUR DATA WITH ETLROBOT', '', 'publish', 'closed', 'closed', '', 'get-your-data-with-etlrobot', '', '', '2018-04-19 12:23:56', '2018-04-19 12:23:56', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=pricingbanner&#038;p=48', 0, 'pricingbanner', '', 0),
(49, 1, '2018-04-19 12:23:54', '2018-04-19 12:23:54', '', 'plan-bg', '', 'inherit', 'open', 'closed', '', 'plan-bg', '', '', '2018-04-19 12:23:54', '2018-04-19 12:23:54', '', 48, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/plan-bg.jpg', 0, 'attachment', 'image/jpeg', 0),
(50, 1, '2018-04-19 12:25:18', '2018-04-19 12:25:18', '$99 / MONTH', 'Basic', '', 'publish', 'closed', 'closed', '', 'basic', '', '', '2018-04-19 12:25:18', '2018-04-19 12:25:18', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=subscriptions&#038;p=50', 0, 'subscriptions', '', 0),
(51, 1, '2018-04-19 12:26:25', '2018-04-19 12:26:25', '$449 / MONTH', 'Advanced', '', 'publish', 'closed', 'closed', '', 'advanced', '', '', '2018-04-19 12:26:25', '2018-04-19 12:26:25', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=subscriptions&#038;p=51', 0, 'subscriptions', '', 0),
(52, 1, '2018-04-19 12:27:57', '2018-04-19 12:27:57', '$899 / MONTH', 'Hero', '', 'publish', 'closed', 'closed', '', 'hero', '', '', '2018-04-19 12:27:57', '2018-04-19 12:27:57', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=subscriptions&#038;p=52', 0, 'subscriptions', '', 0),
(53, 1, '2018-04-19 12:29:15', '2018-04-19 12:29:15', 'CUSTOM PLAN', 'Enterprise', '', 'publish', 'closed', 'closed', '', 'enterprise', '', '', '2018-04-19 12:29:15', '2018-04-19 12:29:15', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=subscriptions&#038;p=53', 0, 'subscriptions', '', 0),
(54, 1, '2018-04-19 12:30:44', '2018-04-19 12:30:44', 'To know more about pricing and features, feel free to contact us.', 'Have Any Queries?', '', 'publish', 'closed', 'closed', '', 'have-any-queries', '', '', '2018-04-19 12:30:44', '2018-04-19 12:30:44', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=pricingquery&#038;p=54', 0, 'pricingquery', '', 0),
(59, 1, '2018-05-08 10:39:54', '2018-05-08 10:39:54', '', 'API', '', 'publish', 'closed', 'closed', '', 'api', '', '', '2018-05-08 10:39:54', '2018-05-08 10:39:54', '', 0, 'http://52.15.241.137/etlrobot_wp/?page_id=59', 0, 'page', '', 0),
(60, 1, '2018-05-08 10:39:54', '2018-05-08 10:39:54', '', 'API', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2018-05-08 10:39:54', '2018-05-08 10:39:54', '', 59, 'http://52.15.241.137/etlrobot_wp/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2018-05-09 13:58:22', '2018-05-09 13:58:22', '', 'abt-img', '', 'inherit', 'open', 'closed', '', 'abt-img-2', '', '', '2018-05-09 13:58:22', '2018-05-09 13:58:22', '', 23, 'http://52.15.241.137/etlrobot_wp/wp-content/uploads/2018/04/abt-img-1.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2018-05-15 07:13:12', '2018-05-15 07:13:12', '', 'Address', '', 'publish', 'closed', 'closed', '', 'address', '', '', '2018-05-15 07:13:12', '2018-05-15 07:13:12', '', 0, 'http://52.15.241.137/etlrobot_wp/?post_type=ft_contact&#038;p=62', 0, 'ft_contact', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE IF NOT EXISTS `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Header Menu', 'header-menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(15, 2, 0),
(16, 2, 0),
(17, 2, 0),
(18, 2, 0),
(19, 2, 0),
(20, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '58'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:13:"220.227.196.0";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'wp_user-settings', 'editor=html&libraryContent=browse'),
(23, 1, 'wp_user-settings-time', '1523618538'),
(24, 1, 'meta-box-order_hpbanner', 'a:3:{s:4:"side";s:9:"submitdiv";s:6:"normal";s:7:"slugdiv";s:8:"advanced";s:40:"Banner Button,attachments-hpBannerAttach";}'),
(25, 1, 'screen_layout_hpbanner', '2'),
(26, 1, 'closedpostboxes_hpbanner', 'a:1:{i:0;s:13:"Banner Button";}'),
(27, 1, 'metaboxhidden_hpbanner', 'a:1:{i:0;s:7:"slugdiv";}'),
(28, 1, 'meta-box-order_hpsection', 'a:3:{s:4:"side";s:22:"submitdiv,postimagediv";s:6:"normal";s:24:"HomePage Section,slugdiv";s:8:"advanced";s:14:"Section Button";}'),
(29, 1, 'screen_layout_hpsection', '2'),
(31, 1, 'session_tokens', 'a:1:{s:64:"469051165557e6baf0fb996c4dda63598801f2cb3875d683a62e0494d0fcae17";a:4:{s:10:"expiration";i:1526531954;s:2:"ip";s:15:"220.227.196.145";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.170 Safari/537.36";s:5:"login";i:1526359154;}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BhUClerctFZpklIpEnV0b1Lir7PGgH0', 'admin', 'sreejith@spericorn.com', '', '2018-04-13 09:00:44', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=308;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=284;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
