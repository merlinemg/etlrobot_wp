<?php

// THUMBNAIL SUPPORT
add_theme_support('post-thumbnails');

// MENU EDITOR
function wpb_custom_new_menu() {
  register_nav_menu('my-custom-menu',__( 'Custom Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );


// Declare ajaxurl GLOBALLY
add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {
   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

function get_data($post_id, $fieldname) {
  global $wpdb;
  $fetch_data = get_post_meta($post_id, $fieldname, true);
  return $fetch_data;
}

// ********************* HOMEPAGE ***************************

// HOMEPAGE BANNER
add_action( 'init', 'hpBanner');
function hpBanner() {
	$args = array(
	    'labels' => array(
	      'name' => __('Home Banners'),
	      'singular_name' => __('Home Banner'),
	      'all_items' => __('All Home Banners'),
	      'add_new_item' => __('Add New Home Banner'),
	      'edit_item' => __('Edit Home Banner'),
	      'view_item' => __('View Home Banner')
	    ),
	    'public' => true,
	    'has_archive' => true,
	    'rewrite' => array('slug' => 'hpBanner'),
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'show_in_nav_menus' => true,
	    'capability_type' => 'page',
	    'supports' => array('title', 'editor', 'thumbnail'),
	    'exclude_from_search' => true,
	    'menu_position' => 80,
	    'has_archive' => true,
	    'menu_icon' => 'dashicons-format-status'
	);

	register_post_type('hpBanner', $args);
}


// class hpBannerVideo_Meta_Box {

//   public function __construct() {

//     if ( is_admin() ) {
//       add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
//       add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
//     }
//   }

//   public function init_metabox() {

//     add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
//     add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

//   }

//   public function add_metabox() {

//     add_meta_box(
//       'Banner Slide Video',
//       __( 'Banner Slide Video', 'text_domain' ),
//       array( $this, 'render_metabox' ),
//       'hpBanner',
//       'advanced',
//       'default');

//   }

//   public function render_metabox( $post ) {

//     wp_nonce_field( 'hpBannerVideo_nonce_action', 'hpBannerVideo_nonce' );

//     $youtube_caption = get_post_meta( $post->ID, 'youtube_caption', true );
//     $youtube_video = get_post_meta( $post->ID, 'youtube_video', true );

//     if( empty( $youtube_caption ) ) $youtube_caption = '';
//     if( empty( $youtube_video ) ) $youtube_video = '';

//     echo '<table class="form-table">';

//     echo '  <tr>';
//     echo '    <th><label for="youtube_caption" class="youtube_caption_label">' . __( 'YouTube Video Caption', 'text_domain' ) . '</label></th>';
//     echo '    <td>';
//     echo '      <input type="text" size="40" id="youtube_caption" name="youtube_caption" class="youtube_caption_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $youtube_caption ) . '">';
//     echo '    </td>';
//     echo '  </tr>';

//     echo '  <tr>';
//     echo '    <th><label for="youtube" class="youtube_label">' . __( 'YouTube Video ID ', 'text_domain' ) . '</label></th>';
//     echo '    <td>';
//     echo '       <input type="text" size="40" id="youtube_video" name="youtube_video" class="youtube_video_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $youtube_video ) . '">';
//     echo '        <p>For Example: https://www.youtube.com/watch?v=XO0Pv2i-RG4</p>';
//     echo '        <p>Its YouTube Video ID is XO0Pv2i-RG4</p>';
//     echo '    </td>';
//     echo '  </tr>';

//     echo '</table>';

//   }

//   public function save_metabox( $post_id, $post ) {

//     $nonce_name   = $_POST['hpBannerVideo_nonce'];
//     $nonce_action = 'hpBannerVideo_nonce_action';

//     if ( ! isset( $nonce_name ) )
//       return;

//     if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
//       return;

//     if ( ! current_user_can( 'edit_post', $post_id ) )
//       return;

//     if ( wp_is_post_autosave( $post_id ) )
//       return;

//     if ( wp_is_post_revision( $post_id ) )
//       return;

//     $newYoutube_caption = isset( $_POST[ 'youtube_caption' ] ) ? $_POST[ 'youtube_caption' ] : '';
//     $newYoutube_video = isset( $_POST[ 'youtube_video' ] ) ? $_POST[ 'youtube_video' ] : '';

//     update_post_meta( $post_id, 'youtube_caption', $newYoutube_caption );
//     update_post_meta( $post_id, 'youtube_video', $newYoutube_video );

//   }

// }
// new hpBannerVideo_Meta_Box;

// ***************************** IMAGE PLUGIN *****************************************
add_filter( 'attachments_default_instance', '__return_false' ); // disable the default instance

add_action( 'attachments_register', 'hpBannerAttach' );
function hpBannerAttach( $attachments ) {
  $fields         = array(
    array(
      'name'      => 'caption',                         // unique field name
      'type'      => 'text',                          // registered field type
      'label'     => __( 'Caption', 'attachments' ),    // label to display
      // 'default'   => 'title',                         // default value upon selection
    ),
  );

  $args = array(
    'label'         => 'Banner SlideShow',
    'post_type'     => array( 'hpBanner' ),
    'position'      => 'normal',
    'priority'      => 'low',
    'filetype'      => null,
    'append'        => true,
    'button_text'   => __( 'Add New Slide', 'attachments' ),
    'modal_text'    => __( 'Add New Slide Image', 'attachments' ),
    'router'        => 'browse',
    'post_parent'   => false,
    'fields'        => $fields,
  );

  $attachments->register( 'hpBannerAttach', $args );
}

//end of HOMEPAGE BANNER

// HOMEPAGE ABOUT
add_action( 'init', 'hpAbout');
function hpAbout() {
	$args = array(
	    'labels' => array(
	      'name' => __('Home About Us'),
	      'singular_name' => __('Home About Us'),
	      'all_items' => __('All Home About Us'),
	      'add_new_item' => __('Add New Home About Us'),
	      'edit_item' => __('Edit Home About Us'),
	      'view_item' => __('View Home About Us')
	    ),
	    'public' => true,
	    'has_archive' => true,
	    'rewrite' => array('slug' => 'hpAbout'),
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'show_in_nav_menus' => true,
	    'capability_type' => 'page',
	    'supports' => array('title', 'editor', 'thumbnail'),
	    'exclude_from_search' => true,
	    'menu_position' => 80,
	    'has_archive' => true,
	    'menu_icon' => 'dashicons-format-status'
	);

	register_post_type('hpAbout', $args);
}

class hpAbout_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'About Us Points',
      __( 'About Us Points', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'hpAbout',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'hpAbout_nonce_action', 'hpAbout_nonce' );

    $text_one = get_post_meta( $post->ID, 'text_one', true );
    $desc_one = get_post_meta( $post->ID, 'desc_one', true );
    $text_two = get_post_meta( $post->ID, 'text_two', true );
    $desc_two = get_post_meta( $post->ID, 'desc_two', true );
    $text_three = get_post_meta( $post->ID, 'text_three', true );
    $desc_three = get_post_meta( $post->ID, 'desc_three', true );
    $text_four = get_post_meta( $post->ID, 'text_four', true );
    $desc_four = get_post_meta( $post->ID, 'desc_four', true );

    if( empty( $text_one ) ) $text_one = '';
    if( empty( $desc_one ) ) $desc_one = '';
    if( empty( $text_two ) ) $text_two = '';
    if( empty( $desc_two ) ) $desc_two = '';
    if( empty( $text_three ) ) $text_three = '';
    if( empty( $desc_three ) ) $desc_three = '';
    if( empty( $text_four ) ) $text_four = '';
    if( empty( $desc_four ) ) $desc_four = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="text_one" class="text_one_label">' . __( 'Point 1 Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="text_one" name="text_one" class="text_one_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $text_one ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="desc_one" class="desc_one_label">' . __( 'Point 1 Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="desc_one" rows="8" cols="42">' . esc_attr__( $desc_one ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="text_two" class="text_two_label">' . __( 'Point 2 Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="text_two" name="text_two" class="text_two_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $text_two ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="desc_two" class="desc_two_label">' . __( 'Point 2 Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="desc_two" rows="8" cols="42">' . esc_attr__( $desc_two ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="text_three" class="text_three_label">' . __( 'Point 3 Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="text_three" name="text_three" class="text_three_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $text_three ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="desc_three" class="desc_three_label">' . __( 'Point 3 Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="desc_three" rows="8" cols="42">' . esc_attr__( $desc_three ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="text_four" class="text_four_label">' . __( 'Point 4 Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="text_four" name="text_four" class="text_four_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $text_four ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="desc_four" class="desc_four_label">' . __( 'Point 4 Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="desc_four" rows="8" cols="42">' . esc_attr__( $desc_four ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';


    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['hpAbout_nonce'];
    $nonce_action = 'hpAbout_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;


    $newText_one = isset( $_POST[ 'text_one' ] ) ? $_POST[ 'text_one' ] : '';
    $newDesc_one = isset( $_POST[ 'desc_one' ] ) ? $_POST[ 'desc_one' ] : '';
    $newText_two = isset( $_POST[ 'text_two' ] ) ? $_POST[ 'text_two' ] : '';
    $newDesc_two = isset( $_POST[ 'desc_two' ] ) ? $_POST[ 'desc_two' ] : '';
    $newText_three = isset( $_POST[ 'text_three' ] ) ? $_POST[ 'text_three' ] : '';
    $newDesc_three = isset( $_POST[ 'desc_three' ] ) ? $_POST[ 'desc_three' ] : '';
    $newText_four = isset( $_POST[ 'text_four' ] ) ? $_POST[ 'text_four' ] : '';
    $newDesc_four = isset( $_POST[ 'desc_four' ] ) ? $_POST[ 'desc_four' ] : '';

    update_post_meta( $post_id, 'text_one', $newText_one );
    update_post_meta( $post_id, 'desc_one', $newDesc_one );
    update_post_meta( $post_id, 'text_two', $newText_two );
    update_post_meta( $post_id, 'desc_two', $newDesc_two );
    update_post_meta( $post_id, 'text_three', $newText_three );
    update_post_meta( $post_id, 'desc_three', $newDesc_three );
    update_post_meta( $post_id, 'text_four', $newText_four );
    update_post_meta( $post_id, 'desc_four', $newDesc_four );

  }

}
new hpAbout_Meta_Box;
// end of HOMEPAGE ABOUT

// HOMEPAGE SECTION
add_action( 'init', 'hpSection');
function hpSection() {
	$args = array(
	    'labels' => array(
	      'name' => __('Home Section'),
	      'singular_name' => __('Home Section'),
	      'all_items' => __('All Home Section'),
	      'add_new_item' => __('Add New Home Section'),
	      'edit_item' => __('Edit Home Section'),
	      'view_item' => __('View Home Section')
	    ),
	    'public' => true,
	    'has_archive' => true,
	    'rewrite' => array('slug' => 'hpSection'),
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'show_in_nav_menus' => true,
	    'capability_type' => 'page',
	    'supports' => array('title', 'editor', 'thumbnail'),
	    'exclude_from_search' => true,
	    'menu_position' => 80,
	    'has_archive' => true,
	    'menu_icon' => 'dashicons-format-status'
	);

	register_post_type('hpSection', $args);
}

class hpSectionImg_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'HomePage Section',
      __( 'HomePage Section', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'hpSection',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'hpSectionImg_nonce_action', 'hpSectionImg_nonce' );

    $trial_text = get_post_meta( $post->ID, 'trial_text', true );
    $img_text = get_post_meta( $post->ID, 'img_text', true );

    if( empty( $trial_text ) ) $trial_text = '';
    if( empty( $img_text ) ) $img_text = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="trial_text" class="trial_text_label">' . __( 'Trial Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="trial_text" name="trial_text" class="trial_text_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $trial_text ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="img_text" class="img_text_label">' . __( 'Image Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="img_text" rows="8" cols="42">' . esc_attr__( $img_text ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['hpSectionImg_nonce'];
    $nonce_action = 'hpSectionImg_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;


    $newTrial_text = isset( $_POST[ 'trial_text' ] ) ? $_POST[ 'trial_text' ] : '';
    $newImg_text = isset( $_POST[ 'img_text' ] ) ? $_POST[ 'img_text' ] : '';

    update_post_meta( $post_id, 'trial_text', $newTrial_text );
    update_post_meta( $post_id, 'img_text', $newImg_text );

  }

}
new hpSectionImg_Meta_Box;

class hpSection_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Section Button',
      __( 'Section Button', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'hpSection',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'hpSection_nonce_action', 'hpSection_nonce' );

    $link_text = get_post_meta( $post->ID, 'link_text', true );
    $link = get_post_meta( $post->ID, 'link', true );

    if( empty( $link_text ) ) $link_text = '';
    if( empty( $link ) ) $link = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="link_text" class="link_text_label">' . __( 'Link Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="link_text" name="link_text" class="link_text_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $link_text ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="link" class="link_label">' . __( 'Location URL', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="link" name="link" class="link_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $link ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['hpSection_nonce'];
    $nonce_action = 'hpSection_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newLink_text = isset( $_POST[ 'link_text' ] ) ? $_POST[ 'link_text' ] : '';
    $newLink = isset( $_POST[ 'link' ] ) ? $_POST[ 'link' ] : '';

    update_post_meta( $post_id, 'link_text', $newLink_text );
    update_post_meta( $post_id, 'link', $newLink );

  }

}
new hpSection_Meta_Box;
// end of HOMEPAGE SECTION

// ABOUT US ASK QUERY
add_action( 'init', 'hpResources');
function hpResources() {
  $args = array(
      'labels' => array(
        'name' => __('Home Data Resources'),
        'singular_name' => __('Home Data Resources'),
        'all_items' => __('All Home Data Resources'),
        'add_new_item' => __('Add New Home Data Resources'),
        'edit_item' => __('Edit Home Data Resources'),
        'view_item' => __('View Home Data Resources')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'hpResources'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('hpResources', $args);
}

// ***************************** IMAGE PLUGIN *****************************************
add_action( 'attachments_register', 'hpAdvertisingAttach' );
function hpAdvertisingAttach( $attachments ) {
  $fields         = array(
    array(
      'name'      => 'url',                         // unique field name
      'type'      => 'text',                          // registered field type
      'label'     => __( 'URL', 'attachments' ),    // label to display
    ),
  );

  $args = array(
    'label'         => 'Data Resources - Advertising',
    'post_type'     => array( 'hpResources' ),
    'position'      => 'normal',
    'priority'      => 'low',
    'filetype'      => null,
    'append'        => true,
    'button_text'   => __( 'Add New Advertising Resource', 'attachments' ),
    'modal_text'    => __( 'Add New Advertising Resource Image', 'attachments' ),
    'router'        => 'browse',
    'post_parent'   => false,
    'fields'        => $fields,
  );

  $attachments->register( 'hpAdvertisingAttach', $args );
}


add_action( 'attachments_register', 'hpSocialAttach' );
function hpSocialAttach( $attachments ) {
  $fields         = array(
    array(
      'name'      => 'url',                         // unique field name
      'type'      => 'text',                          // registered field type
      'label'     => __( 'URL', 'attachments' ),    // label to display
    ),
  );

  $args = array(
    'label'         => 'Data Resources - Social',
    'post_type'     => array( 'hpResources' ),
    'position'      => 'normal',
    'priority'      => 'low',
    'filetype'      => null,
    'append'        => true,
    'button_text'   => __( 'Add New Social Resource', 'attachments' ),
    'modal_text'    => __( 'Add New Social Resource Image', 'attachments' ),
    'router'        => 'browse',
    'post_parent'   => false,
    'fields'        => $fields,
  );

  $attachments->register( 'hpSocialAttach', $args );
}


add_action( 'attachments_register', 'hpAnalyticsAttach' );
function hpAnalyticsAttach( $attachments ) {
  $fields         = array(
    array(
      'name'      => 'url',                         // unique field name
      'type'      => 'text',                          // registered field type
      'label'     => __( 'URL', 'attachments' ),    // label to display
    ),
  );

  $args = array(
    'label'         => 'Data Resources - Analytics',
    'post_type'     => array( 'hpResources' ),
    'position'      => 'normal',
    'priority'      => 'low',
    'filetype'      => null,
    'append'        => true,
    'button_text'   => __( 'Add New Analytics Resource', 'attachments' ),
    'modal_text'    => __( 'Add New Analytics Resource Image', 'attachments' ),
    'router'        => 'browse',
    'post_parent'   => false,
    'fields'        => $fields,
  );

  $attachments->register( 'hpAnalyticsAttach', $args );
}



// ABOUT US ASK QUERY
add_action( 'init', 'hpDemo');
function hpDemo() {
  $args = array(
      'labels' => array(
        'name' => __('Home Request Demo'),
        'singular_name' => __('Home Request Demo'),
        'all_items' => __('All Home Request Demo'),
        'add_new_item' => __('Add New Home Request Demo'),
        'edit_item' => __('Edit Home Request Demo'),
        'view_item' => __('View Home Request Demo')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'hpDemo'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('hpDemo', $args);
}


// ******************************** ABOUT US PAGE ***********************************************
// ABOUT US BANNER
add_action( 'init', 'aboutBanner');
function aboutBanner() {
  $args = array(
      'labels' => array(
        'name' => __('About Page Banners'),
        'singular_name' => __('About Page Banner'),
        'all_items' => __('All About Page Banners'),
        'add_new_item' => __('Add New About Page Banner'),
        'edit_item' => __('Edit About Page Banner'),
        'view_item' => __('View About Page Banner')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'aboutBanner'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor', 'thumbnail'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('aboutBanner', $args);
}

class aboutBanner_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Banner Button',
      __( 'Banner Button', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'aboutBanner',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'aboutBanner_nonce_action', 'aboutBanner_nonce' );

    $link_text = get_post_meta( $post->ID, 'link_text', true );
    $link = get_post_meta( $post->ID, 'link', true );

    if( empty( $link_text ) ) $link_text = '';
    if( empty( $link ) ) $link = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="link_text" class="link_text_label">' . __( 'Link Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="link_text" name="link_text" class="link_text_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $link_text ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="link" class="link_label">' . __( 'Location URL', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="link" name="link" class="link_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $link ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['aboutBanner_nonce'];
    $nonce_action = 'aboutBanner_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newLink_text = isset( $_POST[ 'link_text' ] ) ? $_POST[ 'link_text' ] : '';
    $newLink = isset( $_POST[ 'link' ] ) ? $_POST[ 'link' ] : '';

    update_post_meta( $post_id, 'link_text', $newLink_text );
    update_post_meta( $post_id, 'link', $newLink );

  }

}
new aboutBanner_Meta_Box;


// ABOUT US ABOUT SECTION
add_action( 'init', 'aboutAbout');
function aboutAbout() {
  $args = array(
      'labels' => array(
        'name' => __('About Page About'),
        'singular_name' => __('About Page About'),
        'all_items' => __('All About Page About'),
        'add_new_item' => __('Add New About Page About'),
        'edit_item' => __('Edit About Page About'),
        'view_item' => __('View About Page About')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'aboutAbout'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('aboutAbout', $args);
}

class aboutAbout_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'About Us Points',
      __( 'About Us Points', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'aboutAbout',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'aboutAbout_nonce_action', 'aboutAbout_nonce' );

    $text_one = get_post_meta( $post->ID, 'text_one', true );
    $desc_one = get_post_meta( $post->ID, 'desc_one', true );
    $text_two = get_post_meta( $post->ID, 'text_two', true );
    $desc_two = get_post_meta( $post->ID, 'desc_two', true );
    $text_three = get_post_meta( $post->ID, 'text_three', true );
    $desc_three = get_post_meta( $post->ID, 'desc_three', true );
    $text_four = get_post_meta( $post->ID, 'text_four', true );
    $desc_four = get_post_meta( $post->ID, 'desc_four', true );

    if( empty( $text_one ) ) $text_one = '';
    if( empty( $desc_one ) ) $desc_one = '';
    if( empty( $text_two ) ) $text_two = '';
    if( empty( $desc_two ) ) $desc_two = '';
    if( empty( $text_three ) ) $text_three = '';
    if( empty( $desc_three ) ) $desc_three = '';
    if( empty( $text_four ) ) $text_four = '';
    if( empty( $desc_four ) ) $desc_four = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="text_one" class="text_one_label">' . __( 'Point 1 Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="text_one" name="text_one" class="text_one_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $text_one ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="desc_one" class="desc_one_label">' . __( 'Point 1 Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="desc_one" rows="8" cols="42">' . esc_attr__( $desc_one ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="text_two" class="text_two_label">' . __( 'Point 2 Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="text_two" name="text_two" class="text_two_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $text_two ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="desc_two" class="desc_two_label">' . __( 'Point 2 Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="desc_two" rows="8" cols="42">' . esc_attr__( $desc_two ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="text_three" class="text_three_label">' . __( 'Point 3 Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="text_three" name="text_three" class="text_three_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $text_three ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="desc_three" class="desc_three_label">' . __( 'Point 3 Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="desc_three" rows="8" cols="42">' . esc_attr__( $desc_three ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="text_four" class="text_four_label">' . __( 'Point 4 Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="text_four" name="text_four" class="text_four_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $text_four ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="desc_four" class="desc_four_label">' . __( 'Point 4 Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="desc_four" rows="8" cols="42">' . esc_attr__( $desc_four ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';


    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['aboutAbout_nonce'];
    $nonce_action = 'aboutAbout_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;


    $newText_one = isset( $_POST[ 'text_one' ] ) ? $_POST[ 'text_one' ] : '';
    $newDesc_one = isset( $_POST[ 'desc_one' ] ) ? $_POST[ 'desc_one' ] : '';
    $newText_two = isset( $_POST[ 'text_two' ] ) ? $_POST[ 'text_two' ] : '';
    $newDesc_two = isset( $_POST[ 'desc_two' ] ) ? $_POST[ 'desc_two' ] : '';
    $newText_three = isset( $_POST[ 'text_three' ] ) ? $_POST[ 'text_three' ] : '';
    $newDesc_three = isset( $_POST[ 'desc_three' ] ) ? $_POST[ 'desc_three' ] : '';
    $newText_four = isset( $_POST[ 'text_four' ] ) ? $_POST[ 'text_four' ] : '';
    $newDesc_four = isset( $_POST[ 'desc_four' ] ) ? $_POST[ 'desc_four' ] : '';

    update_post_meta( $post_id, 'text_one', $newText_one );
    update_post_meta( $post_id, 'desc_one', $newDesc_one );
    update_post_meta( $post_id, 'text_two', $newText_two );
    update_post_meta( $post_id, 'desc_two', $newDesc_two );
    update_post_meta( $post_id, 'text_three', $newText_three );
    update_post_meta( $post_id, 'desc_three', $newDesc_three );
    update_post_meta( $post_id, 'text_four', $newText_four );
    update_post_meta( $post_id, 'desc_four', $newDesc_four );

  }

}
new aboutAbout_Meta_Box;


// ABOUT US SECTION
add_action( 'init', 'aboutSection');
function aboutSection() {
  $args = array(
      'labels' => array(
        'name' => __('About Page Section'),
        'singular_name' => __('About Page Section'),
        'all_items' => __('All About Page Section'),
        'add_new_item' => __('Add New About Page Section'),
        'edit_item' => __('Edit About Page Section'),
        'view_item' => __('View About Page Section')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'aboutSection'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor', 'thumbnail'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('aboutSection', $args);
}

class aboutSectionImg_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'About Page Section',
      __( 'About Page Section', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'aboutSection',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'aboutSectionImg_nonce_action', 'aboutSectionImg_nonce' );

    $trial_text = get_post_meta( $post->ID, 'trial_text', true );
    $img_text = get_post_meta( $post->ID, 'img_text', true );

    if( empty( $trial_text ) ) $trial_text = '';
    if( empty( $img_text ) ) $img_text = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="trial_text" class="trial_text_label">' . __( 'Trial Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="trial_text" name="trial_text" class="trial_text_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $trial_text ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="img_text" class="img_text_label">' . __( 'Image Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="img_text" rows="8" cols="42">' . esc_attr__( $img_text ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['aboutSectionImg_nonce'];
    $nonce_action = 'aboutSectionImg_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;


    $newTrial_text = isset( $_POST[ 'trial_text' ] ) ? $_POST[ 'trial_text' ] : '';
    $newImg_text = isset( $_POST[ 'img_text' ] ) ? $_POST[ 'img_text' ] : '';

    update_post_meta( $post_id, 'trial_text', $newTrial_text );
    update_post_meta( $post_id, 'img_text', $newImg_text );

  }

}
new aboutSectionImg_Meta_Box;


class aboutSection_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Section Button',
      __( 'Section Button', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'aboutSection',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'aboutSection_nonce_action', 'aboutSection_nonce' );

    $link_text = get_post_meta( $post->ID, 'link_text', true );
    $link = get_post_meta( $post->ID, 'link', true );

    if( empty( $link_text ) ) $link_text = '';
    if( empty( $link ) ) $link = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="link_text" class="link_text_label">' . __( 'Link Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="link_text" name="link_text" class="link_text_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $link_text ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="link" class="link_label">' . __( 'Location URL', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="link" name="link" class="link_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $link ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['aboutSection_nonce'];
    $nonce_action = 'aboutSection_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newLink_text = isset( $_POST[ 'link_text' ] ) ? $_POST[ 'link_text' ] : '';
    $newLink = isset( $_POST[ 'link' ] ) ? $_POST[ 'link' ] : '';

    update_post_meta( $post_id, 'link_text', $newLink_text );
    update_post_meta( $post_id, 'link', $newLink );

  }

}
new aboutSection_Meta_Box;

// ABOUT US ASK QUERY
add_action( 'init', 'aboutQuery');
function aboutQuery() {
  $args = array(
      'labels' => array(
        'name' => __('About Page Query'),
        'singular_name' => __('About Page Query'),
        'all_items' => __('All About Page Query'),
        'add_new_item' => __('Add New About Page Query'),
        'edit_item' => __('Edit About Page Query'),
        'view_item' => __('View About Page Query')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'aboutQuery'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('aboutQuery', $args);
}

// ************************************* PRICING PAGE **********************************

add_action( 'init', 'pricingBanner');
function pricingBanner() {
  $args = array(
      'labels' => array(
        'name' => __('Pricing Page Banners'),
        'singular_name' => __('Pricing Page Banner'),
        'all_items' => __('All Pricing Page Banners'),
        'add_new_item' => __('Add New Pricing Page Banner'),
        'edit_item' => __('Edit Pricing Page Banner'),
        'view_item' => __('View Pricing Page Banner')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'pricingBanner'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor', 'thumbnail'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('pricingBanner', $args);
}

class pricingBanner_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Banner Button',
      __( 'Banner Button', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'pricingBanner',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'pricingBanner_nonce_action', 'pricingBanner_nonce' );

    $link_text = get_post_meta( $post->ID, 'link_text', true );
    $link = get_post_meta( $post->ID, 'link', true );

    if( empty( $link_text ) ) $link_text = '';
    if( empty( $link ) ) $link = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="link_text" class="link_text_label">' . __( 'Link Text', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="link_text" name="link_text" class="link_text_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $link_text ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="link" class="link_label">' . __( 'Location URL', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="link" name="link" class="link_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $link ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['pricingBanner_nonce'];
    $nonce_action = 'pricingBanner_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newLink_text = isset( $_POST[ 'link_text' ] ) ? $_POST[ 'link_text' ] : '';
    $newLink = isset( $_POST[ 'link' ] ) ? $_POST[ 'link' ] : '';

    update_post_meta( $post_id, 'link_text', $newLink_text );
    update_post_meta( $post_id, 'link', $newLink );

  }

}
new pricingBanner_Meta_Box;

// SUBSCRIPTION TEXT
class pricingSubs_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Subscription Caption',
      __( 'Subscription Caption', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'pricingBanner',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'pricingSubs_nonce_action', 'pricingSubs_nonce' );

    $subCaption = get_post_meta( $post->ID, 'subCaption', true );
    $subDesc = get_post_meta( $post->ID, 'subDesc', true );

    if( empty( $subCaption ) ) $subCaption = '';
    if( empty( $subDesc ) ) $subDesc = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="subCaption" class="subCaption_label">' . __( 'Subscription Caption', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="subCaption" name="subCaption" class="subCaption_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $subCaption ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="subDesc" class="subDesc_label">' . __( 'Subscription Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="subDesc" rows="8" cols="42">' . esc_attr__( $subDesc ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['pricingSubs_nonce'];
    $nonce_action = 'pricingSubs_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newSubCaption = isset( $_POST[ 'subCaption' ] ) ? $_POST[ 'subCaption' ] : '';
    $newSubDesc = isset( $_POST[ 'subDesc' ] ) ? $_POST[ 'subDesc' ] : '';

    update_post_meta( $post_id, 'subCaption', $newSubCaption );
    update_post_meta( $post_id, 'subDesc', $newSubDesc );

  }

}
new pricingSubs_Meta_Box;

// COMPARE FEATURES TEXT
class pricingCompText_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Compare Features Caption',
      __( 'Compare Features Caption', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'pricingBanner',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'comp_action', 'comp' );

    $compCaption = get_post_meta( $post->ID, 'compCaption', true );
    $compDesc = get_post_meta( $post->ID, 'compDesc', true );

    if( empty( $compCaption ) ) $compCaption = '';
    if( empty( $compDesc ) ) $compDesc = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="compCaption" class="compCaption_label">' . __( 'Compare Features Caption', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="compCaption" name="compCaption" class="compCaption_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $compCaption ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="compDesc" class="compDesc_label">' . __( 'Compare Features Description', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="compDesc" rows="8" cols="42">' . esc_attr__( $compDesc ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['comp'];
    $nonce_action = 'comp_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newcompCaption = isset( $_POST[ 'compCaption' ] ) ? $_POST[ 'compCaption' ] : '';
    $newcompDesc = isset( $_POST[ 'compDesc' ] ) ? $_POST[ 'compDesc' ] : '';

    update_post_meta( $post_id, 'compCaption', $newcompCaption );
    update_post_meta( $post_id, 'compDesc', $newcompDesc );

  }

}
new pricingCompText_Meta_Box;


// SUBSCRIPTION PLANS
add_action( 'init', 'subscriptions');
function subscriptions() {
  $args = array(
      'labels' => array(
        'name' => __('Pricing Subscriptions'),
        'singular_name' => __('Pricing Subscription'),
        'all_items' => __('All Pricing Subscriptions'),
        'add_new_item' => __('Add New Pricing Subscription'),
        'edit_item' => __('Edit Pricing Subscription'),
        'view_item' => __('View Pricing Subscription')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'subscriptions'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('subscriptions', $args);
}

// SUBSCRIPTION POINTS
function dynamic_add_subs() {
  add_meta_box( 'subs_box', 'What we do', 'dynamic_inner_subs_box', 'subscriptions');
}
add_action( 'add_meta_boxes', 'dynamic_add_subs' );

function dynamic_inner_subs_box() {
  global $post;
  wp_nonce_field( plugin_basename( __FILE__ ), 'dynamicMeta_noncename' ); ?>
  <div id="meta_inner">

  <?php

  $sub_linkText = get_post_meta( $post->ID, 'sub_linkText', true );
  $sub_link = get_post_meta( $post->ID, 'sub_link', true );
  $sub_boxes = get_post_meta($post->ID,'sub_boxes',false);

  if( empty( $sub_linkText ) ) $sub_linkText = '';
  if( empty( $sub_link ) ) $sub_link = '';

  echo '<table class="form-table">';

  echo '  <tr>';
  echo '    <th><label for="sub_linkText" class="sub_linkText_label">' . __( 'Link Text', 'text_domain' ) . '</label></th>';
  echo '    <td>';
  echo '      <input type="text" size="40" id="sub_linkText" name="sub_linkText" class="sub_linkText_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $sub_linkText ) . '">';
  echo '    </td>';
  echo '  </tr>';

  echo '  <tr>';
  echo '    <th><label for="sub_link" class="sub_link_label">' . __( 'Link Location', 'text_domain' ) . '</label></th>';
  echo '    <td>';
  echo '      <input type="text" size="40" id="sub_link" name="sub_link" class="sub_link_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $sub_link ) . '">';
  echo '    </td>';
  echo '  </tr>';

  echo '</table>';

  $c = 0;
  if ( count( $sub_boxes ) > 0 ) {

    foreach( $sub_boxes[0] as $key => $track ) {?>
      <p>Enter Feature: <input type="text" name="sub_boxes[<?php echo $c; ?>][title]" value="<?php echo $track['title'] ?>" style="width:50%;"/> <span class="remove button button-primary button-large">Remove</span>
      </p>

      <?php
      $c = $c +1;
    }
  }

  ?>

  <span id="box_here"></span>
  <span class="add_box button button-primary button-large"><?php _e('Add New Feature'); ?></span>
  <script>
    var $ =jQuery.noConflict();
    $(document).ready(function() {
      var count = <?php echo $c; ?>;
      $(".add_box").click(function() {
        count = count + 1;

        $('#box_here').append('<p> Enter Title: <input type="text" name="sub_boxes['+count+'][title]" value="" style="width:50%;" /> <span class="remove button button-primary button-large">Remove</span></p>' );
        return false;
      });
      $(".remove").live('click', function() {
        $(this).parent().remove();
      });
    });
  </script>
  </div><?php

}

function dynamic_save_subs_box( $post_id ) {
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
    return;

  if ( !isset( $_POST['dynamicMeta_noncename'] ) )
    return;

  if ( !wp_verify_nonce( $_POST['dynamicMeta_noncename'], plugin_basename( __FILE__ ) ) )
    return;

  $sub_linkText = $_POST['sub_linkText'];
  $sub_link = $_POST['sub_link'];
  $sub_boxes = $_POST['sub_boxes'];

  update_post_meta($post_id,'sub_linkText',$sub_linkText);
  update_post_meta($post_id,'sub_link',$sub_link);
  update_post_meta($post_id,'sub_boxes',$sub_boxes);
}
add_action( 'save_post', 'dynamic_save_subs_box' );


// COMPARE FEATURES
class pricingComp_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Compare Features',
      __( 'Compare Features', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'subscriptions',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'pricingComp_nonce_action', 'pricingComp_nonce' );

    $basePrice = get_post_meta( $post->ID, 'basePrice', true );
    $rows = get_post_meta( $post->ID, 'rows', true );
    $add_rows = get_post_meta( $post->ID, 'add_rows', true );
    $integrations = get_post_meta( $post->ID, 'integrations', true );
    $support = get_post_meta( $post->ID, 'support', true );

    if( empty( $basePrice ) ) $basePrice = '';
    if( empty( $rows ) ) $rows = '';
    if( empty( $add_rows ) ) $add_rows = '';
    if( empty( $integrations ) ) $integrations = '';
    if( empty( $support ) ) $support = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="basePrice" class="basePrice_label">' . __( 'Base Price', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="basePrice" name="basePrice" class="basePrice_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $basePrice ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="rows" class="rows_label">' . __( 'Rows Included', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="rows" name="rows" class="rows_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $rows ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="add_rows" class="add_rows_label">' . __( 'Additional Rows', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="add_rows" name="add_rows" class="add_rows_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $add_rows ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="integrations" class="integrations_label">' . __( 'Integrations', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="integrations" name="integrations" class="integrations_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $integrations ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '  <tr>';
    echo '    <th><label for="support" class="support_label">' . __( 'Support', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="support" name="support" class="support_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $support ) . '">';
    echo '    </td>';
    echo '  </tr>';


    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['pricingComp_nonce'];
    $nonce_action = 'pricingComp_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newbasePrice = isset( $_POST[ 'basePrice' ] ) ? $_POST[ 'basePrice' ] : '';
    $newrows = isset( $_POST[ 'rows' ] ) ? $_POST[ 'rows' ] : '';
    $newadd_rows = isset( $_POST[ 'add_rows' ] ) ? $_POST[ 'add_rows' ] : '';
    $newintegrations = isset( $_POST[ 'integrations' ] ) ? $_POST[ 'integrations' ] : '';
    $newsupport = isset( $_POST[ 'support' ] ) ? $_POST[ 'support' ] : '';

    update_post_meta( $post_id, 'basePrice', $newbasePrice );
    update_post_meta( $post_id, 'rows', $newrows );
    update_post_meta( $post_id, 'add_rows', $newadd_rows );
    update_post_meta( $post_id, 'integrations', $newintegrations );
    update_post_meta( $post_id, 'support', $newsupport );

  }

}
new pricingComp_Meta_Box;

// pricing ASK QUERY
add_action( 'init', 'pricingQuery');
function pricingQuery() {
  $args = array(
      'labels' => array(
        'name' => __('Pricing Query'),
        'singular_name' => __('Pricing Query'),
        'all_items' => __('All Pricing Query'),
        'add_new_item' => __('Add New Pricing Query'),
        'edit_item' => __('Edit Pricing Query'),
        'view_item' => __('View Pricing Query')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'pricingQuery'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title', 'editor'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('pricingQuery', $args);
}


// FOOTER ADDRESS
add_action( 'init', 'ft_Contact');
function ft_Contact() {
  $args = array(
      'labels' => array(
        'name' => __('Footer Contact Details'),
        'singular_name' => __('Footer Contact Details'),
        'all_items' => __('All Footer Contact Details'),
        'add_new_item' => __('Add New Footer Contact Details'),
        'edit_item' => __('Edit Footer Contact Details'),
        'view_item' => __('View Footer Contact Details')
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'ft_Contact'),
      'show_ui' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'capability_type' => 'page',
      'supports' => array('title'),
      'exclude_from_search' => true,
      'menu_position' => 80,
      'has_archive' => true,
      'menu_icon' => 'dashicons-format-status'
  );

  register_post_type('ft_Contact', $args);
}

class ft_Address_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Address',
      __( 'Addresses', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'ft_Contact',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'ft_Address_nonce_action', 'ft_Address_nonce' );

    $ft_address = get_post_meta( $post->ID, 'ft_address', true );

    if( empty( $ft_address ) ) $ft_address = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="ft_address" class="ft_address_label">' . __( 'Enter Address', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <textarea name="ft_address" rows="12" cols="42">' . esc_attr__( $ft_address ) . '</textarea>';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['ft_Address_nonce'];
    $nonce_action = 'ft_Address_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newft_address = isset( $_POST[ 'ft_address' ] ) ? $_POST[ 'ft_address' ] : '';
    update_post_meta( $post_id, 'ft_address', $newft_address );

  }

}
new ft_Address_Meta_Box;


class ft_Phone_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Phone Number',
      __( 'Phone Number', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'ft_Contact',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'ft_Phone_nonce_action', 'ft_Phone_nonce' );

    $ft_phone = get_post_meta( $post->ID, 'ft_phone', true );

    if( empty( $ft_phone ) ) $ft_phone = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="ft_phone" class="ft_Phone_label">' . __( 'Enter Phone Number', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="ft_phone" name="ft_phone" class="ft_phone_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $ft_phone ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['ft_Phone_nonce'];
    $nonce_action = 'ft_Phone_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newft_phone = isset( $_POST[ 'ft_phone' ] ) ? $_POST[ 'ft_phone' ] : '';
    update_post_meta( $post_id, 'ft_phone', $newft_phone );

  }

}
new ft_Phone_Meta_Box;


class ft_Email_Meta_Box {

  public function __construct() {

    if ( is_admin() ) {
      add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
      add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    }
  }

  public function init_metabox() {

    add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
    add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );

  }

  public function add_metabox() {

    add_meta_box(
      'Email Address',
      __( 'Email Address', 'text_domain' ),
      array( $this, 'render_metabox' ),
      'ft_Contact',
      'advanced',
      'default');

  }

  public function render_metabox( $post ) {

    wp_nonce_field( 'ft_Email_nonce_action', 'ft_Email_nonce' );

    $ft_email = get_post_meta( $post->ID, 'ft_email', true );

    if( empty( $ft_email ) ) $ft_email = '';

    echo '<table class="form-table">';

    echo '  <tr>';
    echo '    <th><label for="ft_email" class="ft_Email_label">' . __( 'Enter Email Address', 'text_domain' ) . '</label></th>';
    echo '    <td>';
    echo '      <input type="text" size="40" id="ft_email" name="ft_email" class="ft_email_field" placeholder="' . esc_attr__( '', 'text_domain' ) . '" value="' . esc_attr__( $ft_email ) . '">';
    echo '    </td>';
    echo '  </tr>';

    echo '</table>';

  }

  public function save_metabox( $post_id, $post ) {

    $nonce_name   = $_POST['ft_Email_nonce'];
    $nonce_action = 'ft_Email_nonce_action';

    if ( ! isset( $nonce_name ) )
      return;

    if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
      return;

    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;

    if ( wp_is_post_autosave( $post_id ) )
      return;

    if ( wp_is_post_revision( $post_id ) )
      return;

    $newft_email = isset( $_POST[ 'ft_email' ] ) ? $_POST[ 'ft_email' ] : '';
    update_post_meta( $post_id, 'ft_email', $newft_email );

  }

}
new ft_Email_Meta_Box;



function valid_email($email) {
    return !!filter_var($email, FILTER_VALIDATE_EMAIL);
}

// REQUEST A DEMO
function processDemo() {

  $flag = TRUE;
  $statusArr = array();

  if (isset($_POST['form_data'])) {
      parse_str($_POST['form_data'], $form_data);
  }

  if (empty($form_data['fname'])) {
      $flag = FALSE;
      $statusArr[] = array(status => FALSE, area => "fnameDemo", message => "This field is required");
  }
  if (empty($form_data['lname'])) {
      $flag = FALSE;
      $statusArr[] = array(status => FALSE, area => "lnameDemo", message => "This field is required");
  }
  if (empty($form_data['email'])) {
      $flag = FALSE;
      $statusArr[] = array(status => FALSE,area => "emailDemo", message => "This field is required.");
  } else if (! valid_email($form_data['email'])) {
      $flag = FALSE;
      $statusArr[] = array(status => FALSE, area => "emailDemo", message => "Invalid email Address.");
  }
  if (empty($form_data['subject'])) {
      $flag = FALSE;
      $statusArr[] = array(status => FALSE, area => "subjectDemo", message => "This field is required");
  }
  if (empty($form_data['message'])) {
      $flag = FALSE;
      $statusArr[] = array(status => FALSE, area => "messageDemo", message => "Please enter your Message.");
  }

  if($flag){

    $fname = $form_data['fname'];
    $lname = $form_data['lname'];
    $email = $form_data['email'];
    $subject = $form_data['subject'];
    $message = $form_data['message'];

    $statusArr = array(status => TRUE, message => "Details has been sent successfully!");

  }

  echo json_encode($statusArr);
  die();

}

add_action( 'wp_ajax_processDemo', 'processDemo' );
add_action( 'wp_ajax_nopriv_processDemo', 'processDemo' );

// end of REQUEST A DEMO