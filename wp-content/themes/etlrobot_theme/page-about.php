<?php get_header(); ?>

<header class="bg inner-header">
    <div class="container-fluid custm-hd-wdt hd-brdr">
        <div class="row rw-mb">
            <div class="hd-otr">
                <div class="col-md-2 col-sm-2 col-xs-12 logo-out">
                    <a href="<?php echo get_home_url(); ?>" class="logo"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/etl-logo-white.svg" alt=""></a>
                </div>

                <div class="col-md-10 col-sm-10 col-xs-12">
                    <!--  Nav start here -->
                    <div class="menu-wrp">
                        <div class="close-icn">
                            <div> + </div>
                        </div>
                        <nav class="main-nav">
                            <?php wp_nav_menu(
                                array('menu' => 'Header Menu','container' => '', 'menu_class' => '','min-nav')
                            ); ?>
                        </nav>
                        <div class="mob-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="overlay"></div>
                    </div>
                    <!-- Nav End here -->
                </div>
            </div>
        </div>
    </div>

    <?php

    $aboutBannerArgs = array(
        'post_type' => 'aboutBanner',
        'order' => 'DESC',
        'status' => 'publish',
        'posts_per_page' => 1
    );
    $getAboutBanner = new WP_Query($aboutBannerArgs);

    while ($getAboutBanner->have_posts()) : $getAboutBanner->the_post();
        $currentID = get_the_ID();

        $title = substr(get_the_title($currentID), 0, 100);

        $words = explode(' ', $title);
        $title02 = array_pop($words);
        $title01 = implode(' ', $words);

        $desc = substr(get_post_field('post_content', $currentID), 0, 200);
        $buttonName = get_data($currentID, 'link_text');
        $buttonLink = get_data($currentID, 'link');
        $banner_image = wp_get_attachment_image_src( get_post_thumbnail_id( $currentID ), 'full' );
        ?>

        <div class="container-fluid custm-hd-wdt-bg bg-content">
            <div class="row">

                <div class="col-md-12 colsm-12 col-xs-12 pad-mob bg-hdr-mb-sec">
                    <div class="col-md-5 colsm-5 col-xs-12 bg-bk" style="background: url(<?php echo $banner_image[0]; ?>) no-repeat"></div>
                    <div class="col-md-7 colsm-7 col-xs-12 sldr-cont">
                        <h3>
                            <?php echo $title01; ?> <?php echo $title02; ?>
                        </h3>
                        <h5>
                            <?php echo $desc; ?>
                        </h5>
                        <div class="col-md-12 get_butn">
                            <?php echo $buttonName; ?>
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    <?php endwhile; ?>

</header>

<?php
$aboutAboutArgs = array(
    'post_type' => 'aboutAbout',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$getaboutAbout = new WP_Query($aboutAboutArgs);

while ($getaboutAbout->have_posts()) : $getaboutAbout->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);
    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
    ?>

    <section class="about-inner-sec">
        <div class="col-md-12 colsm-12 col-xs-12  custm-hd-wdt abt-sec-pr">
            <h3> <?php echo $title; ?> </h3>
            <h4>
                <?php echo $desc; ?>
            </h4>
            <div class="col-md-12 colsm-12 col-xs-12  otr-tp-innr">
                <div class="col-md-9 margin_auto wid-abt-inr">
                    <div class="col-md-12 colsm-12 col-xs-12 abt-lef-contn">
                        <div class="diag-abt">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/rbt-01.png" class="img-sec">
                        </div>
                        <div class="abt-cont-dl">
                            <h3>
                                <?php echo get_data($currentID, 'text_one'); ?>
                            </h3>
                            <h4>
                                <?php echo get_data($currentID, 'desc_one'); ?>
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-12 colsm-12 col-xs-12 abt-lef-contn rt-sec">
                        <div class="diag-abt">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/rbt-02.png">
                        </div>
                        <div class="abt-cont-dl">
                            <h3>
                                <?php echo get_data($currentID, 'text_two'); ?>
                            </h3>
                            <h4>
                                <?php echo get_data($currentID, 'desc_two'); ?>
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-12 colsm-12 col-xs-12 abt-lef-contn">
                        <div class="diag-abt">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/rbt-03.png">
                        </div>
                        <div class="abt-cont-dl">
                            <h3>
                                <?php echo get_data($currentID, 'text_three'); ?>
                            </h3>
                            <h4>
                                <?php echo get_data($currentID, 'desc_three'); ?>
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-12 colsm-12 col-xs-12 abt-lef-contn rt-sec">
                        <div class="diag-abt">
                            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/rbt-04.png">
                        </div>
                        <div class="abt-cont-dl">
                            <h3>
                                <?php echo get_data($currentID, 'text_four'); ?>
                            </h3>
                            <h4>
                                <?php echo get_data($currentID, 'desc_four'); ?>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php endwhile; ?>


<?php
$aboutSectionArgs = array(
    'post_type' => 'aboutSection',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$getaboutSection = new WP_Query($aboutSectionArgs);

while ($getaboutSection->have_posts()) : $getaboutSection->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);
    $desc = substr(get_post_field('post_content', $currentID), 0, 200);

    $img_text = get_data($currentID, 'img_text');
    $quarter = (int)ceil(count($words = str_word_count($img_text, 1)) / 4);
    $first = implode(' ', array_slice($words, 0, $quarter));
    $second = implode(' ', array_slice($words, $quarter, $quarter) );
    $third = implode(' ', array_slice($words, ($quarter + $quarter), $quarter) );
    $fourth = implode(' ', array_slice($words, ($quarter + $quarter + $quarter)) );

    $banner_image = wp_get_attachment_image_src( get_post_thumbnail_id( $currentID ), 'full' );
    ?>

    <section class="busins_secn">
        <div class="col-md-12 bg-busin custm-hd-wdt-pr">
            <div class="col-md-6 cont-bus-sec">
                <h3>
                    <?php echo $title; ?>
                </h3>
                <h4>
                    <?php echo $desc; ?>
                </h4>
                <h5>
                    <?php echo get_data($currentID, 'trial_text'); ?>
                </h5>
                <div class="col-md-12 get_butn">
                    <?php echo get_data($currentID, 'link_text'); ?>
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </div>
            </div>
            <div class="col-md-6 bg-busn" style="background: url(<?php echo $banner_image[0]; ?>) no-repeat">
                <div class="img-cont">
                    <h3> <?php echo $first; ?> </h3>
                    <h3> <?php echo $second; ?> </h3>
                    <h3> <?php echo $third; ?> </h3>
                    <h3> <?php echo $fourth; ?> </h3>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>

<?php
$aboutQueryArgs = array(
    'post_type' => 'aboutQuery',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$getaboutQuery = new WP_Query($aboutQueryArgs);

while ($getaboutQuery->have_posts()) : $getaboutQuery->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);
    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
    ?>

    <section class="req-sec abt-sec-req">
        <div class="col-md-12 custm-hd-wdt reqs-sec flip-banner">
            <div class="parallax-overlay">
                <div class="flip-banner-content">
                    <div class="flip-visible">
                        <h3>
                            <?php echo $title; ?>
                        </h3>
                        <h4>
                            <?php echo $desc; ?>
                        </h4>
                    </div>
                    <div class="flip-hidden">
                        <div class="col-md-12 get_butn text-center">
                            Ask Query <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>


<?php get_footer(); ?>