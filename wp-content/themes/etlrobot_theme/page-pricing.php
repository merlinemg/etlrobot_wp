<?php get_header(); ?>

<?php

$pricingBannerArgs = array(
    'post_type' => 'pricingBanner',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$getpricingBanner = new WP_Query($pricingBannerArgs);

while ($getpricingBanner->have_posts()) : $getpricingBanner->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);

    $words = explode(' ', $title);
    $title02 = array_pop($words);
    $title01 = implode(' ', $words);

    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
    $buttonName = get_data($currentID, 'link_text');
    $buttonLink = get_data($currentID, 'link');
    $banner_image = wp_get_attachment_image_src( get_post_thumbnail_id( $currentID ), 'full' );

    $subCaption = get_data($currentID, 'subCaption');
    $subDesc = get_data($currentID, 'subDesc');
    $compCaption = get_data($currentID, 'compCaption');
    $compDesc = get_data($currentID, 'compDesc');

    $half = (int)ceil(count($words = str_word_count($compDesc, 1)) / 2);
    $desc1 = implode(' ', array_slice($words, 0, $half));
    $desc2 = implode(' ', array_slice($words, $half));
    ?>

    <header class="bg bg-pricing" style="background: url(<?php echo $banner_image[0]; ?>) no-repeat">
        <div class="container-fluid custm-hd-wdt hd-brdr">
            <div class="row rw-mb">
                <div class="hd-otr">
                    <div class="col-md-2 col-sm-2 col-xs-12 logo-out">
                        <a href="<?php echo get_home_url(); ?>" class="logo"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/etl-logo.svg" alt=""></a>
                    </div>

                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <!--  Nav start here -->
                        <div class="menu-wrp">

                            <nav class="main-nav">
                                <?php wp_nav_menu(
                                    array('menu' => 'Header Menu','container' => '', 'menu_class' => '','min-nav')
                                ); ?>
                            </nav>

                            <div class="mob-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="overlay"></div>
                        </div>
                        <!-- Nav End here -->
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid custm-hd-wdt-bg bg-content">
            <div class="row">

                <div class="col-md-12 colsm-12 col-xs-12 pad-mob bg-hdr-mb">
                    <div class="col-md-5 colsm-5 col-xs-12"></div>
                    <div class="col-md-7 colsm-7 col-xs-12 sldr-cont">
                        <h3>
                            <?php echo $title01; ?>
                            <span> <?php echo $title02; ?> </span>
                        </h3>
                        <h5>
                            <?php echo $desc; ?>
                        </h5>
                        <div class="col-md-12 get_butn">
                            <?php echo $buttonName; ?>
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </header>

<?php endwhile; ?>


<section class="pricing price-detl">

    <div class="col-md-12 custm-hd-wdt pricing-sec">
        <h3 class="sub-scp">
            <?php echo $subCaption; ?>
        </h3>
        <h4>
            <?php echo $subDesc; ?>
        </h4>

        <div class="col-md-12 pricing-otr">
            <ul class="pricing-cont-detl">
                <?php
                $subscriptionsArgs = array(
                    'post_type' => 'subscriptions',
                    'order' => 'ASC',
                    'status' => 'publish',
                    'posts_per_page' => -1
                );
                $getsubscriptions = new WP_Query($subscriptionsArgs);

                while ($getsubscriptions->have_posts()) : $getsubscriptions->the_post();
                    $currentID = get_the_ID();

                    $title = substr(get_the_title($currentID), 0, 100);
                    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
                    $featuresArr = get_data($currentID, 'sub_boxes');

                    $nameArr[] = $title;
                    $basePriceArr[] = get_data($currentID, 'basePrice');
                    $rowsArr[] = get_data($currentID, 'rows');
                    $add_rowsArr[] = get_data($currentID, 'add_rows');
                    $integrationsArr[] = get_data($currentID, 'integrations');
                    $supportArr[] = get_data($currentID, 'support');

                    ?>
                    <li class="lef">
                        <ul>
                            <li class="pricing-typ">
                                <h4> <?php echo $title; ?></h4>
                                <h3> <?php echo $desc; ?> </h3>
                            </li>
                        </ul>
                        <ul class="pd-lef">
                            <?php foreach ($featuresArr as $key => $value) { ?>
                                <li class="cont-dl">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <span> <?php echo $value['title']; ?> </span>
                                </li>
                            <?php } ?>
                        </ul>
                        <ul>
                            <li class="text-center but-ftb">
                                <button type="button" class="but-prc">
                                    <?php echo get_data($currentID, 'sub_linkText'); ?>
                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                </button>
                            </li>
                        </ul>
                    </li>

                <?php endwhile; ?>

            </ul>
        </div>

    </div>


    <div class="col-md-12 pricng-tabl">
        <div class="col-md-12 custm-hd-wdt pricing-sec">
            <h3 class="sub-scp">
                <?php echo $compCaption; ?>
            </h3>
            <h4>
                <?php echo $desc1; ?> <br/>
                <?php echo $desc2; ?>
            </h4>

            <div class="col-md-12 pricing-otr">
                <table class="table table-bordered table-hover footable">
                    <thead>
                        <tr>
                            <th>Plans</th>
                            <?php foreach ($nameArr as $key => $singleName) {
                                if($key == 1 || $key == 2) { ?>
                                    <th data-hide="phone,tablet"> <?php echo $singleName; ?></th>
                                <?php } else { ?>
                                    <th data-hide="phone"> <?php echo $singleName; ?></th>
                                <?php }
                            } ?>

                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Base Price</td>
                            <?php foreach ($basePriceArr as $singlePrice) { ?>
                                <td> <?php echo $singlePrice; ?> </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Rows Included</td>
                            <?php foreach ($rowsArr as $singleRow) { ?>
                                <td> <?php echo $singleRow; ?> </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Additional Rows</td>
                            <?php foreach ($add_rowsArr as $singleAddrow) { ?>
                                <td> <?php echo $singleAddrow; ?> </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Integrations</td>
                            <?php foreach ($integrationsArr as $singleIntegration) { ?>
                                <td> <?php echo $singleIntegration; ?> </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Support</td>
                            <?php foreach ($supportArr as $singleSupport) { ?>
                                <td> <?php echo $singleSupport; ?> </td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</section>


<?php
$pricingQueryArgs = array(
    'post_type' => 'pricingQuery',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$getpricingQuery = new WP_Query($pricingQueryArgs);

while ($getpricingQuery->have_posts()) : $getpricingQuery->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);
    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
    ?>

    <section class="req-sec abt-sec-req">
        <div class="col-md-12 custm-hd-wdt reqs-sec flip-banner">
            <div class="parallax-overlay">
                <div class="flip-banner-content">
                    <div class="flip-visible">
                        <h3>
                            <?php echo $title; ?>
                        </h3>
                        <h4>
                            <?php echo $desc; ?>
                        </h4>
                    </div>
                    <div class="flip-hidden">
                        <div class="col-md-12 get_butn text-center">
                            Ask Query <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>


<?php get_footer(); ?>