$(document).ready(function() {

    //REMOVE ALL ERROR MESAAGES AND FORM DATA ON EACH MODAL OPEN
    $(".get_butn").click(function(event) {
        clean();
        $("#demoForm")[0].reset();
    });


    $("#sendBtn").click(function(e) {
        e.preventDefault();
        var form_data = $("#demoForm").serialize();
        $.ajax({
            url: ajaxurl,
            dataType: 'json',
            cache: false,
            method: 'post',
            data: {
                'action': 'processDemo',
                'form_data': form_data,
            },
            success: function(data) {
                if (data.status) {
                    clean();
                    $("#demoForm")[0].reset();
                    $("#successDemo").html(data.message);

                    setTimeout(function() {
                        $("#successDemo").html('');
                    }, 2000);

                } else {
                    clean();
                    $.each(data, function(index, value) {
                        $("#" + value.area).html(value.message);
                    });
                }
            },
            error: function(data) {
                console.log('inside error');
            }
        });
    });

});


function clean(){
    $("#fnameDemo").html('');
    $("#lnameDemo").html('');
    $("#emailDemo").html('');
    $("#subjectDemo").html('');
    $("#messageDemo").html('');
    $("#successDemo").html('');
}