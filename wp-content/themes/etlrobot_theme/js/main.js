//Main Menu
$('.main-nav ul li:has(ul)').addClass('submenu');
$('.main-nav ul li:has(ul)').append("<i></i>");
$('.main-nav ul i').click(function() {
    $(this).parent('li').toggleClass('open');
    $(this).parent('li').children('ul').slideToggle();
})

//Mobile Menu
$('.mob-btn').click(function() {
    if (!$('html').hasClass('show-menu')) {
        $('html').addClass('show-menu');
    } else {
        $('html').removeClass('show-menu');
    }
});

$('.overlay').click(function() {
    if ($('html').hasClass('show-menu')) {
        $('html').removeClass('show-menu');
    }
});

//Append and Prepend
$('.first ul').clone().prependTo('.main-nav').addClass('desk-hide')
$('.last ul').clone().appendTo('.main-nav').addClass('desk-hide')



$(function () {

    var filterList = {

        init: function () {

            // MixItUp plugin
            // http://mixitup.io
            $('#portfoliolist').mixItUp({
                selectors: {
              target: '.portfolio',
              filter: '.filter'
          },
          load: {
              filter: '.adv, .social, .analyt' // show app tab on first load
            }
            });

        }

    };

    // Run the show!
    filterList.init();

});


// scroll to section
$(document).ready(function() {
    if ($(window).width() >= 991) {
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            //alert(scroll);

            if (scroll >= 100) {


                $(".nav-sec").css({
                    "position": "fixed",
                    "margin-top": "0%",
                    "top": "0",
                    "background": "#fff",
                    "z-index": "1000",
                    "width": "100%",
                    "padding": "11px 0px 17px",
                    "box-shadow": "1px 1px 2px rgba(85, 78, 78, 0.2)"
                });
                $(".main-nav ul li a").css({
                    "color": "#302e2e",
                    "font-weight": "500"
                });
                $(".hd-otr").css({
                    "margin-top": "0px"
                });
                $(".logo-scrol").css({
                    "display": "block"
                });
                $(".logo-non-scrol").css({
                    "display": "none"
                });
                $(".bg-hdr-mb").css({
                    "margin-top": "125px"
                });

            } else {

                $(".nav-sec").css({
                    "position": "relative",
                    "margin-top": "0",
                    "background": "transparent",
                    "padding": "45px 0px 17px",
                    "box-shadow": "none"
                });
                $(".main-nav ul li a").css({
                    "color": "#fff",
                    "font-weight": "400"
                });
                $(".hd-otr").css({
                    "margin-top": "0px"
                });
                $(".logo-scrol").css({
                    "display": "none"
                });
                $(".logo-non-scrol").css({
                    "display": "block"
                });
                $(".bg-hdr-mb").css({
                    "margin-top": "0px"
                })

            }

        });
    }
});

$(document).ready(function() {
    $(".integration").click(function() {
        $('html,body').stop(true, false).animate({

            scrollTop: $(".data-res-sec").offset().top - 80
        }, 500);
        return false;
    });
    $(".pricing-price").click(function() {
        $('html,body').stop(true, false).animate({

            scrollTop: $(".pricing").offset().top - 80
        }, 1000);
        return false;
    });
    $(".how-work").click(function() {
        $('html,body').stop(true, false).animate({

            scrollTop: $(".busins_secn").offset().top - 80
        }, 1000);
        return false;
    });
    $(".about-us").click(function() {
        $('html,body').stop(true, false).animate({

            scrollTop: $(".about-sectn").offset().top
        }, 1000);
        return false;
    });
});