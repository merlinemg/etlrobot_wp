<?php
$footerArgs = array(
    'post_type' => 'ft_Contact',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$getfooter = new WP_Query($footerArgs);

while ($getfooter->have_posts()) : $getfooter->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 50);
    $address = get_data($currentID, 'ft_address');
    $remngArr = explode(',', $address);
    $last = array_pop($remngArr);
    $first = array_shift($remngArr);
    $middle = implode(', ', $remngArr);

    $phone = get_data($currentID, 'ft_phone');
    $email = get_data($currentID, 'ft_email');

endwhile; ?>

<footer>
    <div class="col-md-12 col-sm-12 col-xs-12 custm-hd-wdt-ft">
        <div class="ftr-otr-mrg">
            <ul class="bdt">
                <li class="ft-lef">
                    <ul class="adr-brdr">
                        <li class="hed"> <?php echo $title; ?></li>
                        <li class="adr">
                            <?php echo $first; ?>
                            <br/> <?php echo $middle; ?>
                            <br/> <?php echo $last; ?>

                            <br/> Phone: <?php echo $phone; ?>
                            <br/> Email: <?php echo $email; ?>

                        </li>
                        <li></li>
                    </ul>
                </li>
                <li class="ft-lef">
                    <ul class="ser-pg">
                        <li class="hed">Services</li>
                        <li>
                            Home Page
                        </li>
                        <li>
                            Terms of Service
                        </li>
                        <li>
                            Privacy Policy
                        </li>

                    </ul>
                </li>
                <li class="ft-lef">
                    <ul class="ser-pg mbt-ftr-btm">
                        <li class="hed">Services</li>
                        <li>
                            Home Page
                        </li>
                        <li>
                            Terms of Service
                        </li>
                        <li>
                            Privacy Policy
                        </li>

                    </ul>
                </li>
                <li class="ft-lef zib">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/img/zib.jpg">
                </li>
            </ul>
        </div>

    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 bg-copy">
        <div class="col-md-7 margin_auto">
            <div class="col-md-12">
                <div class="col-md-12 col-sm-12 col-xs-12 ft-img text-center">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-ft.png">
                </div>
                <!-- <div class="col-md-12 col-sm-12 col-xs-12 ft-cont text-center">
                    ETL stands for Extract, Transform, and Load. ETLRobot is a Cloud service ETL engine to simplify and automate the of capturing and structuring your data and then loading it to your data warehouse.
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 social-icn">
                    <ul>
                        <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                        <li class="bg1"><i class="fa fa-youtube" aria-hidden="true"></i></li>
                        <li class="bg2"><i class="fa fa-instagram" aria-hidden="true"></i></li>
                        <li class="bg3"><i class="fa fa-twitter" aria-hidden="true"></i></li>
                        <li class="bg4"><i class="fa fa-tumblr" aria-hidden="true"></i></li>
                    </ul>
                </div> -->
                <div class="col-md-12 col-sm-12 col-xs-12 copy text-center">
                    Copyright © 2018 ETL Robot. All rights reserved.
                </div>
            </div>
        </div>
    </div>
</footer>


<!-- JQUERY -->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/vendor/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.mixitup.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/main.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>

<script type="text/javascript">
    // get video element id
    var vidClip = document.getElementById("myVideo");

    // play video event
    function playVid() {
        myVideo.play();
    }

    // pause video event
    function pauseVid() {
        myVideo.pause();
    }

    // toggle button class on click
    $('button').on('click', function() {
        $('.first, .second').toggle();
    });

    // toggle button class when finished
    vidClip.onended = function(e) {
        $('.first, .second').toggle();
    };
</script>

<!-- footable -->
<script type="text/javascript">
    $(function() {
        $('.footable').footable({
            calculateWidthOverride: function() {
                return {
                    width: $(window).width()
                };

            }
        });
    });
</script>
<!-- footable end -->

<script src="<?php bloginfo('stylesheet_directory'); ?>/js/footable.js"></script>


</body>
</html>