<?php

if(function_exists($_GET['func'])) {
   $funcName = $_GET['func']();
}


function getSubscriptions(){
	$subscriptionsArgs = array(
	    'post_type' => 'subscriptions',
	    'order' => 'ASC',
	    'status' => 'publish',
	    'posts_per_page' => -1
	);
	$getsubscriptions = new WP_Query($subscriptionsArgs);

	while ($getsubscriptions->have_posts()) : $getsubscriptions->the_post();
	    $currentID = get_the_ID();

	    $title = get_the_title($currentID);
	    $desc = get_post_field('post_content', $currentID);
	    $featuresArr = get_data($currentID, 'sub_boxes');

	    $tempArr[] = array("title" => $title, "description" => $desc, "featuresArr" => $featuresArr);

	endwhile;

	echo json_encode($tempArr);
	die();

}


function getPlansTable(){

	$subscriptionsArgs = array(
	    'post_type' => 'subscriptions',
	    'order' => 'ASC',
	    'status' => 'publish',
	    'posts_per_page' => -1
	);
	$getsubscriptions = new WP_Query($subscriptionsArgs);

	while ($getsubscriptions->have_posts()) : $getsubscriptions->the_post();
	    $currentID = get_the_ID();

	    $name = get_the_title($currentID);
	    $basePrice = get_data($currentID, 'basePrice');
	    $rows = get_data($currentID, 'rows');
	    $add_rows = get_data($currentID, 'add_rows');
	    $integrations = get_data($currentID, 'integrations');
	    $support = get_data($currentID, 'support');

	    $tempArr[] = array("plan_name" => $name, "basePrice" => $basePrice, "rows" => $rows, "add_rows" => $add_rows, "integrations" => $integrations, "support" => $support );

	endwhile;

	echo json_encode($tempArr);
	die();

}