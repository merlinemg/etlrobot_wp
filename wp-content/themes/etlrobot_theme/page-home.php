<?php get_header(); ?>
<?php

$hpBannerArgs = array(
    'post_type' => 'hpBanner',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$gethpBanner = new WP_Query($hpBannerArgs);

while ($gethpBanner->have_posts()) : $gethpBanner->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);

    $quarter = (int)floor(count($words = str_word_count($title, 1)) / 3);
    $title01 = implode(' ', array_slice($words, 0, $quarter));
    $title02 = implode(' ', array_slice($words, $quarter, $quarter ));
    $title03 = implode(' ', array_slice($words, ($quarter + $quarter)) );


    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
    $buttonName = get_data($currentID, 'link_text');
    $buttonLink = get_data($currentID, 'link');
    $banner_image = wp_get_attachment_image_src( get_post_thumbnail_id( $currentID ), 'full' );

    $subCaption = get_data($currentID, 'subCaption');
    $subDesc = get_data($currentID, 'subDesc');

    $attachments = new Attachments( 'hpBannerAttach');
    $firstImg = $attachments->url(0);
    $firstCap = $attachments->field('caption', 0 );
    $thirdImg = $attachments->url(2);
    $thirdCap = $attachments->field('caption', 2 );
    $fourthImg = $attachments->url(3);
    $fourthCap = $attachments->field('caption', 3 );

    $secondImg = $attachments->url(1);
    $secondCap = $attachments->field('caption', 1 );
    ?>

    <header class="bg">
        <div class="nav-sec">
            <div class="container-fluid custm-hd-wdt hd-brdr">
                <div class="row rw-mb">
                    <div class="hd-otr">
                        <div class="col-md-3 col-sm-2 col-xs-12 logo-out">
                            <a href="<?php echo get_home_url(); ?>" class="logo logo-non-scrol">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/img/etl-logo.svg" alt="Logo">
                            </a>
                            <a href="<?php echo get_home_url(); ?>" class="logo logo-scrol">
                                <img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-ft.png" alt="Logo">
                            </a>
                        </div>

                        <div class="col-md-9 col-sm-10 col-xs-12">
                            <!--  Nav start here -->
                            <div class="menu-wrp">

                                <nav class="main-nav">
                                    <?php wp_nav_menu(
                                        array('menu' => 'Header Menu','container' => '', 'menu_class' => '','min-nav')
                                    ); ?>

                                    <ul class="req-ul">
                                        <li>
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#requestModal">
                                                Request a Demo
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="mob-btn">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="overlay"></div>
                            </div>
                            <!-- Nav End here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid custm-hd-wdt-bg bg-content">
            <div class="row">

                <div class="col-md-12 colsm-12 col-xs-12 pad-mob bg-hdr-mb">
                    <div class="col-md-5 colsm-5 col-xs-12 bg-bk"></div>
                    <div class="col-md-7 colsm-7 col-xs-12 sldr-cont">

                        <h3> <?php echo $title01; ?>
                            <span> <?php echo $title02; ?> </span> <?php echo $title03; ?>
                        </h3>

                        <h5>
                            <?php echo $desc; ?>
                        </h5>
                        <a href="https://app.etlrobot.com/register">
                            <!-- <div class="col-md-12 get_butn" data-toggle="modal" data-target="#requestModal"> -->
                            <div class="col-md-12 get_butn">
                                GET STARTED
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                            </div>
                        </a>
                    </div>

                </div>
            </div>

        </div>

    </header>

    <section class="smp_cont">
        <div class="col-md-12 custm-hd-wdt">
            <div class="col-md-3 smp_bg" style="background: url(<?php echo $firstImg; ?>)">
                <div class="smp_cont_txt">
                    <h3> <?php echo $firstCap; ?> </h3>
                </div>
            </div>
            <!-- <div class="col-md-3 smp_vid">
                <video height="240" width="100%">
                    <source src="<?php bloginfo('stylesheet_directory'); ?>/video/mov_bbb.mp4" type="video/mp4">
                    <source src="<?php bloginfo('stylesheet_directory'); ?>/video/mov_bbb.ogg" type="video/ogg">
                    Your browser does not support HTML5 video.
                </video>
                <div class="smp_cont_txt">
                    <h3> SAMPLE DUMMY VIDEO </h3>
                </div>
                <div class="buttons">
                    <button class="uk-button uk-button-primary" type="button" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-play"></i></button>
                </div>
            </div> -->
            <div class="col-md-3 smp_bg bg1" style="background: url(<?php echo $secondImg; ?>)">
                <div class="smp_cont_txt">
                    <h3> <?php echo $secondCap; ?> </h3>
                </div>
            </div>
            <div class="col-md-3 smp_bg bg2" style="background: url(<?php echo $thirdImg; ?>)">
                <div class="smp_cont_txt">
                    <h3> <?php echo $thirdCap; ?> </h3>
                </div>
            </div>
            <div class="col-md-3 smp_bg bg3" style="background: url(<?php echo $fourthImg; ?>)">
                <div class="smp_cont_txt">
                    <h3> <?php echo $fourthCap; ?> </h3>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>

<?php
$hpAboutArgs = array(
    'post_type' => 'hpAbout',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$gethpAbout = new WP_Query($hpAboutArgs);

while ($gethpAbout->have_posts()) : $gethpAbout->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);
    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
    $banner_image = wp_get_attachment_image_src( get_post_thumbnail_id( $currentID ), 'full' );
    ?>

    <section class="about-sectn">
        <div class="col-md-12 custm-hd-wdt-bg abt-sec-pr">
            <h3>
                <?php echo $title; ?>
            </h3>
            <h4>
                <?php echo $desc; ?>
            </h4>

            <div class="col-md-12 abt-otr">
                <div class="col-md-4 otr-sec-abt">
                    <div class="col-md-12 pad_out lef-abt-qn">
                        <div class="col-md-9 abt-cont-qn">
                            <h3>
                                <?php echo get_data($currentID, 'text_one'); ?>
                            </h3>
                            <h4>
                                <?php echo get_data($currentID, 'desc_one'); ?>
                            </h4>
                        </div>
                        <div class="col-md-3 line-sec-abt"></div>
                    </div>
                    <div class="col-md-12 pad_out lef-abt-qn tpr-abt">
                        <div class="col-md-9 abt-cont-qn">
                            <h3>
                                <?php echo get_data($currentID, 'text_two'); ?>
                            </h3>
                            <h4>
                                <?php echo get_data($currentID, 'desc_two'); ?>
                            </h4>
                        </div>
                        <div class="col-md-3 line-sec-abt-tp">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 otr-sec-abt text-center">
                    <img src="<?php echo $banner_image[0]; ?>">
                </div>
                <div class="col-md-4 otr-sec-abt">
                    <div class="col-md-12 pad_out rt-abt-qn">
                        <div class="col-md-3 line-sec-abt-rt"></div>
                        <div class="col-md-9 abt-cont-qn-rt">
                            <h3>
                                <?php echo get_data($currentID, 'text_three'); ?>
                            </h3>
                            <h4>
                                <?php echo get_data($currentID, 'desc_three'); ?>
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-12 pad_out rt-abt-qn tpr-abt">
                        <div class="col-md-3 line-sec-abt-tp">
                            <span></span>
                        </div>
                        <div class="col-md-9 abt-cont-qn-rt">
                            <h3>
                                <?php echo get_data($currentID, 'text_four'); ?>
                            </h3>
                            <h4>
                                <?php echo get_data($currentID, 'desc_four'); ?>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>


<?php
$hpSectionArgs = array(
    'post_type' => 'hpSection',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$gethpSection = new WP_Query($hpSectionArgs);

while ($gethpSection->have_posts()) : $gethpSection->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);
    $desc = substr(get_post_field('post_content', $currentID), 0, 200);

    $img_text = get_data($currentID, 'img_text');
    $quarter = (int)ceil(count($words = str_word_count($img_text, 1)) / 4);
    $first = implode(' ', array_slice($words, 0, $quarter));
    $second = implode(' ', array_slice($words, $quarter, $quarter) );
    $third = implode(' ', array_slice($words, ($quarter + $quarter), $quarter) );
    $fourth = implode(' ', array_slice($words, ($quarter + $quarter + $quarter)) );

    $banner_image = wp_get_attachment_image_src( get_post_thumbnail_id( $currentID ), 'full' );
    ?>

    <section class="busins_secn">
        <div class="col-md-12 bg-busin custm-hd-wdt-pr">
            <div class="col-md-6 cont-bus-sec">
                <h3>
                    <?php echo $title; ?>
                </h3>
                <h4>
                    <?php echo $desc; ?>
                </h4>
                <h5>
                    <?php echo get_data($currentID, 'trial_text'); ?>
                </h5>
                <a href="<?php echo get_data($currentID, 'link'); ?>">
                    <div class="col-md-12 get_butn">
                        <?php echo get_data($currentID, 'link_text'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </div>
                </a>
            </div>
            <div class="col-md-6 bg-busn" style="background: url(<?php echo $banner_image[0]; ?>) no-repeat">
                <div class="img-cont">
                    <h3> <?php echo $first; ?> </h3>
                    <h3> <?php echo $second; ?> </h3>
                    <h3> <?php echo $third; ?> </h3>
                    <h3> <?php echo $fourth; ?> </h3>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>


<?php
$pricingBannerArgs = array(
    'post_type' => 'pricingBanner',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$getpricingBanner = new WP_Query($pricingBannerArgs);

while ($getpricingBanner->have_posts()) : $getpricingBanner->the_post();
    $currentID = get_the_ID();
    $subCaption = get_data($currentID, 'subCaption');
    $subDesc = get_data($currentID, 'subDesc');
endwhile; ?>

<section class="pricing">
    <div class="col-md-12 custm-hd-wdt pricing-sec">
        <h3 class="sub-scp">
            <?php echo $subCaption; ?>
        </h3>
        <h4>
            <?php echo $subDesc; ?>
        </h4>

        <div class="col-md-12 pricing-otr">
            <ul class="pricing-cont-detl">
                <?php
                $subscriptionsArgs = array(
                    'post_type' => 'subscriptions',
                    'order' => 'ASC',
                    'status' => 'publish',
                    'posts_per_page' => -1
                );
                $getsubscriptions = new WP_Query($subscriptionsArgs);

                while ($getsubscriptions->have_posts()) : $getsubscriptions->the_post();
                    $currentID = get_the_ID();

                    $title = substr(get_the_title($currentID), 0, 100);
                    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
                    $featuresArr = get_data($currentID, 'sub_boxes');
                    ?>
                    <li class="lef">
                        <ul>
                            <li class="pricing-typ">
                                <h4> <?php echo $title; ?></h4>
                                <h3> <?php echo $desc; ?> </h3>
                            </li>
                        </ul>
                        <ul class="pd-lef">
                            <?php foreach ($featuresArr as $key => $value) { ?>
                                <li class="cont-dl">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <span> <?php echo $value['title']; ?> </span>
                                </li>
                            <?php } ?>
                        </ul>
                        <ul>
                            <li class="text-center">
                                <a href="<?php echo get_data($currentID, 'sub_link'); ?>">
                                    <button type="button" class="but-prc">
                                        <?php echo get_data($currentID, 'sub_linkText'); ?>
                                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                    </button>
                                </a>
                            </li>
                        </ul>
                    </li>

                <?php endwhile; ?>

            </ul>
        </div>

    </div>
</section>

<?php
$hpResourcesArgs = array(
    'post_type' => 'hpResources',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$gethpResources = new WP_Query($hpResourcesArgs);

while ($gethpResources->have_posts()) : $gethpResources->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);
    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
    $half = (int)ceil(count($words = str_word_count($desc, 1)) / 2);
    $desc1 = implode(' ', array_slice($words, 0, $half));
    $desc2 = implode(' ', array_slice($words, $half));

    $hpAdvertisingAttach = new Attachments( 'hpAdvertisingAttach');
    $hpSocialAttach = new Attachments( 'hpSocialAttach');
    $hpAnalyticsAttach = new Attachments( 'hpAnalyticsAttach');

    ?>

    <section class="data-res-sec">
        <div class="col-md-12 custm-hd-wdt resource-sec">
            <h3> <?php echo $title; ?> </h3>
            <!-- <h4> -->
                <?php
                    // echo $desc1;
                ?>
                <!-- <br> -->
                <?php
                    // echo $desc2;
                ?>
            <!-- </h4> -->
            <div class="otr-pot col-md-12">
                <ul id="filters" class="clearfix">
                    <li><span class="filter active" data-filter=".adv, .social, .analyt">All</span></li>
                    <li><span class="filter" data-filter=".adv">Advertising</span></li>
                    <li><span class="filter" data-filter=".social">Social</span></li>
                    <li><span class="filter" data-filter=".analyt">Analytics</span></li>

                </ul>
                <div id="portfoliolist">
                    <?php while( $hpAdvertisingAttach->get() ) : ?>
                        <a href="<?php echo $hpAdvertisingAttach->field('url'); ?>">
                            <div class="portfolio adv" data-cat="adv">
                                <div class="portfolio-wrapper">
                                    <img src="<?php echo $hpAdvertisingAttach->url(); ?>" />

                                </div>
                            </div>
                        </a>
                    <?php endwhile; ?>

                    <?php while( $hpSocialAttach->get() ) : ?>
                        <a href="<?php echo $hpSocialAttach->field('url'); ?>">
                            <div class="portfolio social" data-cat="social">
                                <div class="portfolio-wrapper">
                                    <img src="<?php echo $hpSocialAttach->url(); ?>" />

                                </div>
                            </div>
                        </a>
                    <?php endwhile; ?>

                    <?php while( $hpAnalyticsAttach->get() ) : ?>
                        <a href="<?php echo $hpAnalyticsAttach->field('url'); ?>">
                            <div class="portfolio analyt" data-cat="analyt">
                                <div class="portfolio-wrapper">
                                    <img src="<?php echo $hpAnalyticsAttach->url(); ?>" />

                                </div>
                            </div>
                        </a>
                    <?php endwhile; ?>

                </div>

            </div>
        </div>
    </section>

<?php endwhile; ?>


<?php
$hpDemoArgs = array(
    'post_type' => 'hpDemo',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$gethpDemo = new WP_Query($hpDemoArgs);

while ($gethpDemo->have_posts()) : $gethpDemo->the_post();
    $currentID = get_the_ID();

    $title = substr(get_the_title($currentID), 0, 100);
    $desc = substr(get_post_field('post_content', $currentID), 0, 200);
    $half = (int)ceil(count($words = str_word_count($desc, 1)) / 2);
    $desc1 = implode(' ', array_slice($words, 0, $half));
    $desc2 = implode(' ', array_slice($words, $half));
endwhile;

$footerArgs = array(
    'post_type' => 'ft_Contact',
    'order' => 'DESC',
    'status' => 'publish',
    'posts_per_page' => 1
);
$getfooter = new WP_Query($footerArgs);

while ($getfooter->have_posts()) : $getfooter->the_post();
    $currentID = get_the_ID();

    $address = get_data($currentID, 'ft_address');
    $phone = get_data($currentID, 'ft_phone');
    $email = get_data($currentID, 'ft_email');

endwhile; ?>

<section class="req-sec">
    <div class="col-md-12 custm-hd-wdt reqs-sec flip-banner">
        <div class="parallax-overlay">
            <div class="flip-banner-content">
                <div class="flip-visible">
                    <h3>
                        <?php echo $title; ?>
                    </h3>
                    <h4>
                        <?php echo $desc1; ?> <br>
                        <?php echo $desc2; ?>
                    </h4>
                    <div class="col-md-12 con-dtl-otr">
                        <div class="col-md-8 margin_auto">
                            <div class="col-md-4 icn">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <h4>Address</h4>
                                <h5> <?php echo $address; ?> </h5>
                            </div>
                            <div class="col-md-4 icn">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <h4>Phone Number</h4>
                                <h5> <?php echo $phone; ?> </h5>
                            </div>
                            <div class="col-md-4 icn">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <h4>Email</h4>
                                <h5> <?php echo $email; ?> </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flip-hidden">
                    <div class="col-md-12 get_butn text-center" data-toggle="modal" data-target="#requestModal">
                        GET STARTED <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- modal -->
<div id="requestModal" class="modal fade mod-demo" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Request Demo</h4>
                <div class="col-md-1 col-sm-1 col-xs-1 margin_auto brd"></div>
                <h5>
                    To schedule a product demo with one of our product consultants please fill in your contact details
                    <br/><br/><span style="color: green;" id="successDemo"></span>
                </h5>
            </div>
            <form id="demoForm" action="" method="POST">
                <div class="modal-body">
                    <div class="col-md-12 pad-mob">
                        <div class="col-md-6 inp-sec">
                            <input type="text" class="form-control" placeholder="First Name" name="fname">
                            <span class="error-red" id="fnameDemo"></span>
                        </div>
                        <div class="col-md-6 inp-sec">
                            <input type="text" class="form-control" placeholder="Last Name" name="lname">
                            <span class="error-red" id="lnameDemo"></span>
                        </div>
                        <div class="col-md-6 inp-sec">
                            <input type="text" class="form-control" placeholder="Email" name="email">
                            <span class="error-red" id="emailDemo"></span>
                        </div>
                        <div class="col-md-6 inp-sec">
                            <input type="text" class="form-control" placeholder="Subject" name="subject">
                            <span class="error-red" id="subjectDemo"></span>
                        </div>
                        <div class="col-md-12 inp-sec">
                            <textarea class="form-control" placeholder="Message" name="message"></textarea>
                            <span class="error-red" id="messageDemo"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="sendBtn">Send Message</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal modal-video-sec" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body vid-mdl">
                <video id="myVideo" height="100%" width="100%">
                    <source src="<?php bloginfo('stylesheet_directory'); ?>/video/mov_bbb.mp4" type="video/mp4">
                    <source src="<?php bloginfo('stylesheet_directory'); ?>/video/mov_bbb.ogg" type="video/ogg">
                    Your browser does not support HTML5 video.
                </video>
                <div class="buttons">
                    <button class="second" onclick="pauseVid()" type="button">
                        <i class="fa fa-pause"></i>
                    </button>
                    <button class="uk-button uk-button-primary first" onclick="playVid()" type="button">
                        <i class="fa fa-play"></i>
                    </button>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>