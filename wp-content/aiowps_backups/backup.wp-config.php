<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'etlrobot_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'y@2*dJS@UuSp');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AOK)8 leOuIXR@-`)RVpXfl5}v84`}h.;*S<4Y[,21OdzJ+&(K~R+G,YPV6T8g)4');
define('SECURE_AUTH_KEY',  'AlbiR.KM!SHFO]2{gjO=}*qG1wCT//@tvKrptpB|qbA&|T7FWedXOS*[2.*}8_j5');
define('LOGGED_IN_KEY',    'iDSFeagDdxx&AYY=hr3}-4=&kLE}fix)aePh=UqLp?%kv|`ER8PDKS?wNJ:iDs;V');
define('NONCE_KEY',        '^78n14_*!0Xv;bR1,8>*r2#oUB5e7l,3F)#pi}#f~enn`D(3WCzuwkDL$`TL??9#');
define('AUTH_SALT',        'a#HT2px|M5aQDbfeCD99}AD~)>f@f;q3J N^Zu1^>Cai%M lE@2jcv#PjQ^q.(od');
define('SECURE_AUTH_SALT', '}Kqi=HISHK{0u424dQ*TK^rpnuW)Ose{2vyt:wHO0TSK759asFxDMo0ueBId&3oj');
define('LOGGED_IN_SALT',   'dCW8DEQZFJ~f[I_q6+Gj4L2C,I_)5zw792+!jG<9Sj7+$iyU[Z{,3|9&w?$ul#tx');
define('NONCE_SALT',       'HSbUBV5CX]C]`CH{=as(t_+Kbxy->>s0+yV#7e!~Hr|u1LXTEopp0i)staqfN6oA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
